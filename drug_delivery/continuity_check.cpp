#include <MoFEM.hpp>
#include <OpContCheck.hpp>

using namespace MoFEM;
using namespace Example;

static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  // // initialize petsc
  // MoFEM::Core::Initialize(&argc, &argv, (char *)0, help);

  // try {

  //   // Create MoAB database
  //   moab::Core moab_core;
  //   moab::Interface &moab = moab_core;

  //   // Create MoFEM database and link it to MoAB
  //   MoFEM::Core mofem_core(moab);
  //   MoFEM::Interface &m_field = mofem_core;

  //   // Register DM Manager
  //   DMType dm_name = "DMMOFEM";
  //   CHKERR DMRegister_MoFEM(dm_name);

  //   // Simple interface
  //   Simple *simple_interface;
  //   CHKERR m_field.getInterface(simple_interface);
  //   {
  //     // get options from command line
  //     CHKERR simple_interface->getOptions();
  //     // load mesh file
  //     CHKERR simple_interface->loadFile("");

  //     auto get_base = []() -> FieldApproximationBase {
  //       enum bases { AINSWORTH, DEMKOWICZ, LASTBASEOP };
  //       const char *list_bases[] = {"ainsworth", "demkowicz"};
  //       PetscBool flg;
  //       PetscInt choice_base_value = AINSWORTH;
  //       CHKERR PetscOptionsGetEList(PETSC_NULL, NULL, "-base", list_bases,
  //                                   LASTBASEOP, &choice_base_value, &flg);
  //       if (flg == PETSC_TRUE) {
  //         FieldApproximationBase base = AINSWORTH_LEGENDRE_BASE;
  //         if (choice_base_value == AINSWORTH)
  //           base = AINSWORTH_LEGENDRE_BASE;
  //         else if (choice_base_value == DEMKOWICZ)
  //           base = DEMKOWICZ_JACOBI_BASE;
  //         return base;
  //       }
  //       return LASTBASE;
  //     };

  //     // add fields
  //     auto base = get_base();
  //     CHKERR simple_interface->addDomainField("FIELD", HCURL, base, 1);
  //     CHKERR simple_interface->addSkeletonField("FIELD", HCURL, base, 1);
  //     // set fields order
  //     CHKERR simple_interface->setFieldOrder("FIELD", 2);
  //     // setup problem
  //     CHKERR simple_interface->setUp();
  //     // get dm
  //     auto dm = simple_interface->getDM();

  //     // create elements
  //     CommonData elem_data;
  //     boost::shared_ptr<EdgeEle> skeleton_fe =
  //         boost::shared_ptr<EdgeEle>(new EdgeEle(m_field));

  //     skeleton_fe->getOpPtrVector().push_back(
  //         new OpSetContrariantPiolaTransformOnEdge());
  //     skeleton_fe->getOpPtrVector().push_back(
  //         new SkeletonFE(m_field, elem_data));

  //     // iterate skeleton finite elements
  //     CHKERR DMoFEMLoopFiniteElements(dm, simple_interface->getSkeletonFEName(),
  //                                     skeleton_fe);
  //   }
  // }
  // CATCH_ERRORS;

  // // finish work cleaning memory, getting statistics, etc.
  // MoFEM::Core::Finalize();

  return 0;
}