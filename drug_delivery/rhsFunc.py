def rhs_func(a, b):
  return 1.0 * a * (1.0 - b) - 1.0 * b


def diffusivity(domain):
  if(domain == "DOMAIN_1"):
    return 0.5
  if(domain == "DOMAIN_2"):
    return 0.1

