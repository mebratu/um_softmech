# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src)

# Add tests
#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/atom_tests)

set(permissions_default 
  OWNER_WRITE 
  OWNER_READ
  GROUP_READ)
set(permissions_execute 
  OWNER_WRITE 
  OWNER_READ
  GROUP_READ
  OWNER_EXECUTE 
  GROUP_EXECUTE)


softmech_copy_and_install("*.msh" "${permissions_default}")
softmech_copy_and_install("*.cfg" "${permissions_default}")
softmech_copy_and_install("*.jou" "${permissions_default}")
softmech_copy_and_install("*.cub" "${permissions_default}")
softmech_copy_and_install("*.h5m" "${permissions_default}")
softmech_copy_and_install("*.sh" "${permissions_execute}")
softmech_copy_and_install("*.petsc" "${permissions_execute}")
softmech_copy_and_install("*.geo" "${permissions_execute}")


softmech_build_and_install(
  unsatu2dFlow_prob ${CMAKE_CURRENT_SOURCE_DIR}/unsatu2dFlow_prob.cpp)


