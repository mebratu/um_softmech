#!/bin/bash
  
rm -rf out*

if [[ $1 = "-np" || $3 = "-order" ]]; then
        /Users/mebratuwakeni/mofem_install/um_release/build/tools/mofem_part -my_file mesh.cub -output_file mesh.h5m -my_nparts $2 -dim 2 -adj_dim 1

        make -j$2 -C /Users/mebratuwakeni/mofem_install/um_debug/build/softmech/poisson_problem

        time mpirun -np $2 /Users/mebratuwakeni/mofem_install/um_debug/build/softmech/poisson_problem/poisson_prob -file_name mesh.h5m -order $4 2>&1 | tee log
elif [[ $1 = "-order" ]]; then
        /Users/mebratuwakeni/mofem_install/um_debug/build/tools/mofem_part -my_file mesh.cub -output_file mesh.h5m -my_nparts 1 -dim 2 -adj_dim 1

        make -j1 -C /Users/mebratuwakeni/mofem_install/um_debug/build/softmech/poisson_problem

        time mpirun -np 1 /Users/mebratuwakeni/mofem_install/um_debug/build/softmech/poisson_problem/poisson_prob -file_name mesh.h5m -order $2 2>&1 | tee log
fi


convert.py -np 6 out_level*