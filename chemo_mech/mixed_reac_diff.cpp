#include <stdlib.h>
#include <BasicFiniteElements.hpp>

Tag thQuadRule;
Tag thMaxOrder;
int min_order;
int roghness = 0;
int adaptive = 0;
#include <RDOperators.hpp>
#include <ErrorEstimateDrafts.hpp>

using namespace MoFEM;
using namespace ReactionDiffusion;
using namespace ErrorEstimates;

static char help[] = "...\n\n";

// #define M_PI 3.14159265358979323846 /* pi */

struct RDProblem {
public:
  RDProblem(MoFEM::Core &core) 
  : m_field(core)
  , cOmm(m_field.get_comm())
  , rAnk(m_field.get_comm_rank()) {
    vol_ele_slow_rhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
    natural_bdry_ele_slow_rhs =
        boost::shared_ptr<EdgeEle>(new EdgeEle(m_field));
    vol_ele_stiff_rhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
    vol_ele_stiff_lhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));

    skeleton_fe = boost::shared_ptr<EdgeEle>(new EdgeEle(m_field));
    max_post_error_fe = boost::shared_ptr<FaceEle>(new FaceEle(m_field));

    post_proc = boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D>(
        new PostProcFaceOnRefinedMeshFor2D(m_field));

    data1 = boost::shared_ptr<PreviousData>(new PreviousData());
    data2 = boost::shared_ptr<PreviousData>(new PreviousData());
    data3 = boost::shared_ptr<PreviousData>(new PreviousData());

    flux_values_ptr1 =
        boost::shared_ptr<MatrixDouble>(data1, &data1->flux_values);

    flux_divs_ptr1 = boost::shared_ptr<VectorDouble>(data1, &data1->flux_divs);

    mass_values_ptr1 =
        boost::shared_ptr<VectorDouble>(data1, &data1->mass_values);

    mass_dots_ptr1 = boost::shared_ptr<VectorDouble>(data1, &data1->mass_dots);

    flux_values_ptr2 =
        boost::shared_ptr<MatrixDouble>(data2, &data2->flux_values);
    flux_divs_ptr2 = boost::shared_ptr<VectorDouble>(data2, &data2->flux_divs);

    mass_values_ptr2 =
        boost::shared_ptr<VectorDouble>(data2, &data2->mass_values);
    mass_dots_ptr2 = boost::shared_ptr<VectorDouble>(data2, &data2->mass_dots);

    flux_values_ptr3 =
        boost::shared_ptr<MatrixDouble>(data3, &data3->flux_values);
    flux_divs_ptr3 = boost::shared_ptr<VectorDouble>(data3, &data3->flux_divs);

    mass_values_ptr3 =
        boost::shared_ptr<VectorDouble>(data3, &data3->mass_values);

    mass_dots_ptr3 = boost::shared_ptr<VectorDouble>(data3, &data3->mass_dots);
  }

  // RDProblem(const int order) : order(order){}
  MoFEMErrorCode run_analysis(int nb_sp);

  double global_error0;
  double global_error1;
  double post_error;
  double maxError;

private:
  MoFEMErrorCode setup_system();
  MoFEMErrorCode add_fe(std::string mass_field, std::string flux_field);
  MoFEMErrorCode set_blockData(std::map<int, BlockData> &block_data_map);
  MoFEMErrorCode extract_bd_ents(std::string ESSENTIAL, std::string NATURAL, std::string internal);
  MoFEMErrorCode extract_initial_ents(int block_id, Range &surface);
  MoFEMErrorCode update_slow_rhs(std::string mass_fiedl,
                                 boost::shared_ptr<VectorDouble> &mass_ptr);
  MoFEMErrorCode push_slow_rhs(std::string mass_field, std::string flux_field,
                               boost::shared_ptr<PreviousData> &data);
  MoFEMErrorCode update_vol_fe(boost::shared_ptr<FaceEle> &vol_ele,
                               boost::shared_ptr<PreviousData> &data);
  MoFEMErrorCode
  update_stiff_rhs(std::string mass_field, std::string flux_field,
                   boost::shared_ptr<VectorDouble> &mass_ptr,
                   boost::shared_ptr<MatrixDouble> &flux_ptr,
                   boost::shared_ptr<VectorDouble> &mass_dot_ptr,
                   boost::shared_ptr<VectorDouble> &flux_div_ptr);
  MoFEMErrorCode push_stiff_rhs(std::string mass_field, std::string flux_field,
                                boost::shared_ptr<PreviousData> &data,
                                std::map<int, BlockData> &block_map);
  MoFEMErrorCode update_stiff_lhs(std::string mass_fiedl,
                                  std::string flux_field,
                                  boost::shared_ptr<VectorDouble> &mass_ptr,
                                  boost::shared_ptr<MatrixDouble> &flux_ptr);
  MoFEMErrorCode push_stiff_lhs(std::string mass_field, std::string flux_field,
                                boost::shared_ptr<PreviousData> &data,
                                std::map<int, BlockData> &block_map);

  MoFEMErrorCode set_integration_rule();
  MoFEMErrorCode apply_IC(std::string mass_field, Range &surface,
                          boost::shared_ptr<PreviousData> &data,
                          boost::shared_ptr<FaceEle> &initial_ele);
  MoFEMErrorCode apply_BC(std::string flux_field);
  MoFEMErrorCode loop_fe();
  MoFEMErrorCode post_proc_fields(std::string mass_field,
                                  std::string flux_field);
  MoFEMErrorCode output_result();
  MoFEMErrorCode solve();

  MoFEM::Interface &m_field;
  Simple *simple_interface;
  SmartPetscObj<DM> dm;
  SmartPetscObj<TS> ts;

  Range essential_bdry_ents;
  Range natural_bdry_ents;

  Range internal_edge_ents;

  Range inner_surface1; // nb_species times
  Range inner_surface2;
  Range inner_surface3;

  MPI_Comm cOmm;
  const int rAnk;

  int nb_species;



  std::map<int, BlockData> material_blocks;

  boost::shared_ptr<FaceEle> vol_ele_slow_rhs;
  boost::shared_ptr<FaceEle> vol_ele_stiff_rhs;
  boost::shared_ptr<FaceEle> vol_ele_stiff_lhs;
  boost::shared_ptr<EdgeEle> natural_bdry_ele_slow_rhs;

  boost::shared_ptr<EdgeEle> skeleton_fe;
  boost::shared_ptr<FaceEle> max_post_error_fe;

  boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D> post_proc;
  boost::shared_ptr<Monitor> monitor_ptr;

  boost::shared_ptr<PreviousData> data1; // nb_species times
  boost::shared_ptr<PreviousData> data2;
  boost::shared_ptr<PreviousData> data3;

  boost::shared_ptr<MatrixDouble> flux_values_ptr1; // nb_species times
  boost::shared_ptr<MatrixDouble> flux_values_ptr2;
  boost::shared_ptr<MatrixDouble> flux_values_ptr3;

  boost::shared_ptr<VectorDouble> flux_divs_ptr1; // nb_species times
  boost::shared_ptr<VectorDouble> flux_divs_ptr2;
  boost::shared_ptr<VectorDouble> flux_divs_ptr3;

  boost::shared_ptr<VectorDouble> mass_values_ptr1; // nb_species times
  boost::shared_ptr<VectorDouble> mass_values_ptr2;
  boost::shared_ptr<VectorDouble> mass_values_ptr3;

  boost::shared_ptr<VectorDouble> mass_dots_ptr1; // nb_species times
  boost::shared_ptr<VectorDouble> mass_dots_ptr2;
  boost::shared_ptr<VectorDouble> mass_dots_ptr3;

  boost::shared_ptr<ForcesAndSourcesCore> null;
};

const double ramp_t = 1.0;
const double sml = 0.0;
const double T = 3.0 * M_PI / 2.0;
const double R = 0.75;

struct KinkFunction {
  double operator() (const double x) const {
    return 1 - abs(x);
  }
};

struct DerKinkFunction{
  double operator()(const double x) const {
    if(x > 0)
      {return -1;}
    else 
      {return 1;}
}
};

struct ExactFunction {
  double operator()(const double x, const double y, const double t) const {

    double g = 0;
    if (roghness == 0) {
      g = cos(T * x) * cos(T * y);
    } else {
      double r = x * x + y * y;
      double d = R * R - r;

      if (d > 0.0) {
        g = exp(-r / d);
      }
    }

    if (t <= ramp_t) {
      return g * t;
    } else {
      return g * ramp_t;
    }
  }
};

struct ExactFunctionGrad {
  FTensor::Tensor1<double, 3> operator()(const double x, const double y,
                                         const double t) const {
    FTensor::Tensor1<double, 3> grad;
    double mx = 0.0;
    double my = 0.0;
    if (roghness == 0) {
      mx = - T * sin(T * x) * cos(T * y);
      my = - T * cos(T * x) * sin(T * y);
    } else {
      double r = x * x + y * y;
      double d = R * R - r;
      if (d > 0.0) {
        double rx = 2.0 * x;
        double ry = 2.0 * y;

        double g = exp(-r / d);

        mx = g * (-rx * R * R / (d * d));
        my = g * (-ry * R * R / (d * d));
      }
    }

    if (t <= ramp_t) {
      grad(0) = mx * t;
      grad(1) = my * t;
    } else {
      grad(0) = mx * ramp_t;
      grad(1) = my * ramp_t;
    }
    grad(2) = 0.0;
    return grad;
  }
};

struct ExactFunctionLap {
  double operator()(const double x, const double y, const double t) const {

    double glap = 0.0;
    if(roghness == 0){
      glap = -2.0 * pow(T, 2) * cos(T * x) * cos(T * y);
    } else{
      double r = x * x + y * y;
      double d = R * R - r;
      if (d > 0.0) {
        double rx = 2.0 * x;
        double ry = 2.0 * y;

        double rxx = 2.0;
        double ryy = 2.0;
        double g = exp(-r / d);

        double cof = R * R / (d * d);
        glap = g * ((-rx * cof) * (-rx * cof) + (-ry * cof) * (-ry * cof) -
                    R * R * (rxx * d + 2.0 * rx * rx) / (d * d * d) -
                    R * R * (ryy * d + 2.0 * ry * ry) / (d * d * d));
      }
    }
   
    if (t <= ramp_t) {
      return glap * t;
    } else {
      return glap * ramp_t;
    }
  }
};

struct ExactFunctionDot {
  double operator()(const double x, const double y, const double t) const {
    double gdot = 0;
    if(roghness == 0){
      gdot = cos(T * x) * cos(T * y);
    } else {
      double r = x * x + y * y;
      double d = R * R - r;

      if (d > 0.0) {
        gdot = exp(-r / d);
      }
    }
    
    if (t <= ramp_t) {
      return gdot;
    } else {
      return 0;
    }
  }
};

MoFEMErrorCode RDProblem::setup_system() {
  MoFEMFunctionBegin;
  CHKERR m_field.getInterface(simple_interface);
  CHKERR simple_interface->getOptions();
  CHKERR simple_interface->loadFile();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::add_fe(std::string mass_field,
                                 std::string flux_field) {
  MoFEMFunctionBegin;
  
  
  CHKERR simple_interface->addDomainField(mass_field, L2, AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simple_interface->addDomainField(flux_field, HCURL, AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simple_interface->addSkeletonField(flux_field, HCURL, DEMKOWICZ_JACOBI_BASE, 1);

  // CHKERR simple_interface->addDataField("U", HCURL, DEMKOWICZ_JACOBI_BASE, 1);

  // CHKERR simple_interface->addBoundaryField(flux_field, HCURL, DEMKOWICZ_JACOBI_BASE, 1);

  
  CHKERR simple_interface->setFieldOrder(mass_field, g_max_order - 1);
  CHKERR simple_interface->setFieldOrder(flux_field, g_max_order);
  // CHKERR simple_interface->setFieldOrder("U", order - 1);


  MoFEMFunctionReturn(0);
}
MoFEMErrorCode RDProblem::set_blockData(std::map<int, BlockData> &block_map) {
  MoFEMFunctionBegin;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
    string name = it->getName();
    const int id = it->getMeshsetId();
    if (name.compare(0, 14, "REGION1") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 1e-3;
      block_map[id].block_id = id;

    } else if (name.compare(0, 14, "REGION2") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 1e-2;
      block_map[id].block_id = id;
    } else if (name.compare(0, 14, "REGION3") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 5e-2;
      block_map[id].block_id = id;
    } else if (name.compare(0, 14, "REGION4") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 1e-1;
      block_map[id].block_id = id;
    } else if (name.compare(0, 14, "REGION5") == 0) {
      CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, block_map[id].block_ents, true);
      block_map[id].B0 = 5e-1;
      block_map[id].block_id = id;
    }
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::extract_bd_ents(std::string essential,
                                          std::string natural,
                                          std::string internal) {
  MoFEMFunctionBegin;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
    string name = it->getName();
    if (name.compare(0, 14, natural) == 0) {

      CHKERR it->getMeshsetIdEntitiesByDimension(m_field.get_moab(), 1,
                                                 natural_bdry_ents, true);
    } else if (name.compare(0, 14, essential) == 0) {
      CHKERR it->getMeshsetIdEntitiesByDimension(m_field.get_moab(), 1,
                                                 essential_bdry_ents, true);
    } else if (name.compare(0, 14, internal) == 0) {
      CHKERR it->getMeshsetIdEntitiesByDimension(m_field.get_moab(), 1,
                                                 internal_edge_ents, true);
    }
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::extract_initial_ents(int block_id, Range &surface) {
  MoFEMFunctionBegin;
  if (m_field.getInterface<MeshsetsManager>()->checkMeshset(block_id,
                                                            BLOCKSET)) {
    CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
        block_id, BLOCKSET, 2, surface, true);
  }
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode
RDProblem::update_slow_rhs(std::string mass_field,
                           boost::shared_ptr<VectorDouble> &mass_ptr) {
  MoFEMFunctionBegin;
  vol_ele_slow_rhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(mass_field, mass_ptr, MBTRI));
  MoFEMFunctionReturn(0);
}

// MoFEMErrorCode RDProblem::push_slow_rhs(std::string mass_field,
//                                         std::string flux_field,
//                                         boost::shared_ptr<PreviousData> &data1) {
//   MoFEMFunctionBegin;

//   vol_ele_slow_rhs->getOpPtrVector().push_back(
//       new OpAssembleSlowRhsV(mass_field, data, 
//                              ExactFunction(), 
//                              ExactFunctionDot(), 
//                              ExactFunctionLap(),
//                              m_field));

//   // natural_bdry_ele_slow_rhs->getOpPtrVector().push_back(
//   //     new OpSkeletonSource(mass_field, 
//   //                          KinkFunction(),
//   //                          ExactFunction(), 
//   //                          internal_edge_ents));

//   // natural_bdry_ele_slow_rhs->getOpPtrVector().push_back(
//   //     new OpAssembleNaturalBCRhsTau(flux_field, natural_bdry_ents));

//   // natural_bdry_ele_slow_rhs->getOpPtrVector().push_back(
//   //     new OpEssentialBC(flux_field, essential_bdry_ents));
//   MoFEMFunctionReturn(0);
// }

MoFEMErrorCode RDProblem::update_vol_fe(boost::shared_ptr<FaceEle> &vol_ele,
                                        boost::shared_ptr<PreviousData> &data) {
  MoFEMFunctionBegin;
  vol_ele->getOpPtrVector().push_back(
    new OpCalculateJacForFace(data->jac));
  vol_ele->getOpPtrVector().push_back(
    new OpCalculateInvJacForFace(data->inv_jac));
  vol_ele->getOpPtrVector().push_back(
    new OpMakeHdivFromHcurl());

  vol_ele->getOpPtrVector().push_back(
      new OpSetContravariantPiolaTransformFace(data->jac));

  vol_ele->getOpPtrVector().push_back(
    new OpSetInvJacHcurlFace(data->inv_jac));
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode
RDProblem::update_stiff_rhs(std::string mass_field, std::string flux_field,
                            boost::shared_ptr<VectorDouble> &mass_ptr,
                            boost::shared_ptr<MatrixDouble> &flux_ptr,
                            boost::shared_ptr<VectorDouble> &mass_dot_ptr,
                            boost::shared_ptr<VectorDouble> &flux_div_ptr) {

  MoFEMFunctionBegin;

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(mass_field, mass_ptr, MBTRI));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorField<3>(flux_field, flux_ptr));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot(mass_field, mass_dot_ptr, MBTRI));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorDivergence<3, 2>(flux_field, flux_div_ptr));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::push_stiff_rhs(std::string mass_field,
                                         std::string flux_field,
                                         boost::shared_ptr<PreviousData> &data,
                                         std::map<int, BlockData> &block_map) {
  MoFEMFunctionBegin;
  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpAssembleStiffRhsTau<3>(flux_field, data, block_map, m_field));

  vol_ele_stiff_rhs->getOpPtrVector().push_back(
      new OpAssembleStiffRhsV<3>(mass_field, data, block_map, ExactFunction(),
                                 ExactFunctionDot(), ExactFunctionLap(), m_field));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
RDProblem::update_stiff_lhs(std::string mass_field, std::string flux_field,
                            boost::shared_ptr<VectorDouble> &mass_ptr,
                            boost::shared_ptr<MatrixDouble> &flux_ptr) {
  MoFEMFunctionBegin;
  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues(mass_field, mass_ptr));

  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpCalculateHdivVectorField<3>(flux_field, flux_ptr));    
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::push_stiff_lhs(std::string mass_field,
                                         std::string flux_field,
                                         boost::shared_ptr<PreviousData> &data,
                                         std::map<int, BlockData> &block_map) {
  MoFEMFunctionBegin;
  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpAssembleLhsTauTau<3>(flux_field, data, block_map, m_field));

  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpAssembleLhsVV(mass_field, thMaxOrder, m_field));

  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpAssembleLhsTauV<3>(flux_field, mass_field, data, block_map, m_field));

  vol_ele_stiff_lhs->getOpPtrVector().push_back(
      new OpAssembleLhsVTau(mass_field, flux_field, thMaxOrder, m_field));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::set_integration_rule() {
  MoFEMFunctionBegin;
  auto vol_ele_slow_rhs_rule = [&](int, int, int) -> int {
    auto fe_ent = vol_ele_slow_rhs->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(thQuadRule, &fe_ent, 1, &quad_rule);

    return 2 * (quad_rule + 0);
  };
  vol_ele_slow_rhs->getRuleHook = vol_ele_slow_rhs_rule;

  // ====================================================

  auto natural_bdry_ele_slow_rhs_rule = [&](int, int, int) -> int {
    auto fe_ent = natural_bdry_ele_slow_rhs->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(thQuadRule, &fe_ent, 1, &quad_rule);

    return 2 * (quad_rule + 0);
  };
  natural_bdry_ele_slow_rhs->getRuleHook = natural_bdry_ele_slow_rhs_rule;

  // ====================================================

  auto vol_ele_stiff_rhs_rule = [&](int, int, int) -> int {
    auto fe_ent = vol_ele_stiff_rhs->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(thQuadRule, &fe_ent, 1, &quad_rule);

    return 2 * (quad_rule + 0);
  };
  vol_ele_stiff_rhs->getRuleHook = vol_ele_stiff_rhs_rule;

  // ====================================================
  auto vol_ele_stiff_lhs_rule = [&](int, int, int) -> int {
    auto fe_ent = vol_ele_stiff_lhs->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(thQuadRule, &fe_ent, 1, &quad_rule);

    return 2 * (quad_rule + 0);
  };
  vol_ele_stiff_lhs->getRuleHook = vol_ele_stiff_lhs_rule;

  // ====================================================

  auto skeleton_fe_rule = [&](int, int, int) -> int {
    auto fe_ent = skeleton_fe->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(thMaxOrder, &fe_ent, 1,
                                           &quad_rule);

    return 2 * (quad_rule + 0);
  };
  skeleton_fe->getRuleHook = skeleton_fe_rule;

  // ====================================================
  // domain_error_fe->getRuleHook = vol_rule;
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::apply_IC(std::string mass_field, Range &surface,
                                   boost::shared_ptr<PreviousData> &data,
                                   boost::shared_ptr<FaceEle> &initial_ele) {
  MoFEMFunctionBegin;
  initial_ele->getOpPtrVector().push_back(
      new OpInitialMass(mass_field, data, surface, m_field));
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::apply_BC(std::string flux_field) {
  MoFEMFunctionBegin;
  CHKERR m_field.getInterface<ProblemsManager>()->removeDofsOnEntities(
      simple_interface->getProblemName(), flux_field, essential_bdry_ents);

  MoFEMFunctionReturn(0);
}
MoFEMErrorCode RDProblem::loop_fe() {
  MoFEMFunctionBegin;



  CHKERR TSSetType(ts, TSARKIMEX);
  CHKERR TSARKIMEXSetType(ts, TSARKIMEXA2);

  CHKERR DMMoFEMTSSetIJacobian(dm, simple_interface->getDomainFEName(),
                               vol_ele_stiff_lhs, null, null);


  CHKERR DMMoFEMTSSetIFunction(dm, simple_interface->getDomainFEName(),
                               vol_ele_stiff_rhs, null, null);

  // CHKERR DMMoFEMTSSetRHSFunction(dm, simple_interface->getDomainFEName(),
  //                                vol_ele_slow_rhs, null, null);

  CHKERR DMMoFEMTSSetRHSFunction(dm, simple_interface->getSkeletonFEName(),
                                 skeleton_fe, null, null);


  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::post_proc_fields(std::string mass_field,
                                           std::string flux_field) {
  MoFEMFunctionBegin;
  post_proc->addFieldValuesPostProc(mass_field);
  post_proc->addFieldValuesPostProc(flux_field);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::output_result() {
  MoFEMFunctionBegin;
  CHKERR DMMoFEMTSSetMonitor(dm, ts, simple_interface->getDomainFEName(),
                             monitor_ptr, null, null);
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode RDProblem::solve() {
  MoFEMFunctionBegin;
  // Create solution vector
  SmartPetscObj<Vec> X;
  CHKERR DMCreateGlobalVector_MoFEM(dm, X);
  CHKERR DMoFEMMeshToLocalVector(dm, X, INSERT_VALUES, SCATTER_FORWARD);
  // Solve problem
  double ftime = 1;
  CHKERR TSSetDM(ts, dm);
  CHKERR TSSetMaxTime(ts, ftime);
  CHKERR TSSetSolution(ts, X);
  CHKERR TSSetFromOptions(ts);

  if (1) {
    SNES snes;
    CHKERR TSGetSNES(ts, &snes);
    KSP ksp;
    CHKERR SNESGetKSP(snes, &ksp);
    PC pc;
    CHKERR KSPGetPC(ksp, &pc);
    PetscBool is_pcfs = PETSC_FALSE;
    PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);
    // Set up FIELDSPLIT
    // Only is user set -pc_type fieldsplit
    if (is_pcfs == PETSC_TRUE) {
      IS is_mass1, is_flux1;
      IS is_mass2, is_flux2;
      IS is_mass3, is_flux3;

      const MoFEM::Problem *problem_ptr;
      CHKERR DMMoFEMGetProblemPtr(dm, &problem_ptr);
      CHKERR
      m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "MASS1", 0, 1, &is_mass1);
      CHKERR
      m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "FLUX1", 0, 1, &is_flux1);
      CHKERR
      m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "MASS2", 0, 1, &is_mass2);
      CHKERR
      m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "FLUX2", 0, 1, &is_flux2);
      CHKERR
      m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "MASS3", 0, 1, &is_mass3);
      CHKERR
      m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "FLUX3", 0, 1, &is_flux3);
      // CHKERR ISView(is_flux, PETSC_VIEWER_STDOUT_SELF);
      // CHKERR ISView(is_mass, PETSC_VIEWER_STDOUT_SELF);
      
      CHKERR PCFieldSplitSetIS(pc, NULL, is_mass1);
      CHKERR PCFieldSplitSetIS(pc, NULL, is_flux1);

      CHKERR PCFieldSplitSetIS(pc, NULL, is_mass2);
      CHKERR PCFieldSplitSetIS(pc, NULL, is_flux2);

      CHKERR PCFieldSplitSetIS(pc, NULL, is_mass3);
      CHKERR PCFieldSplitSetIS(pc, NULL, is_flux3);

      CHKERR ISDestroy(&is_flux1);
      CHKERR ISDestroy(&is_mass1);

      CHKERR ISDestroy(&is_flux2);
      CHKERR ISDestroy(&is_mass2);

      CHKERR ISDestroy(&is_flux3);
      CHKERR ISDestroy(&is_mass3);
  }
}

    // auto field_split_fun = [](TS ts_ctx) -> PetscErrorCode {
    //   MoFEMFunctionBegin;

    //   DM dm;
    //   CHKERR TSGetDM(ts_ctx, &dm);

    //   MoFEM::Interface *m_field_ptr;

    //   CHKERR DMoFEMGetInterfacePtr(dm, &m_field_ptr);

    //   SNES snes;
    //   CHKERR TSGetSNES(ts_ctx, &snes);
    //   KSP ksp;
    //   CHKERR SNESGetKSP(snes, &ksp);
    //   PC pc;
    //   CHKERR KSPGetPC(ksp, &pc);
    //   PetscBool is_pcfs = PETSC_FALSE;
    //   PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);

    //   if(is_pcfs){

    //     IS is_lo;
    //     IS is_hi;
    //     const MoFEM::Problem *problem_ptr;
    //     CHKERR DMMoFEMGetProblemPtr(dm, &problem_ptr);
    //     auto dofs = problem_ptr->getNumeredRowDofs();
    //     auto nb_local_dofs = problem_ptr->getNbLocalDofsRow();
    //     Tag tag;
    //     CHKERR m_field_ptr->get_moab().tag_get_handle("_ORDER", tag);

    //     std::vector<int> vec_is_lo;
    //     std::vector<int> vec_is_hi;

    //     vec_is_lo.reserve(dofs->size());
    //     vec_is_hi.reserve(dofs->size());

    //     for (auto &dof : *(dofs)) {
    //       auto loc_idx = dof->getPetscLocalDofIdx();

    //       if (loc_idx < nb_local_dofs) {
    //         auto ent = dof->getEnt();
    //         int tag_order;
    //         CHKERR m_field_ptr->get_moab().tag_get_data(tag, &ent, 1,
    //         &tag_order); auto o = dof->getDofOrder();

    //         bool correct_order = false;

    //         if(dof->getSpace() == L2){

    //             correct_order = (o < tag_order);
    //         } else {

    //             correct_order = (o <= tag_order);
    //         }

    //         if (correct_order){
    //           vec_is_lo.push_back(dof->getPetscGlobalDofIdx());
    //         } else {
    //           vec_is_hi.push_back(dof->getPetscGlobalDofIdx());
    //         }

    //       }
    //     }
    //     std::sort(vec_is_lo.begin(), vec_is_lo.end());
    //     CHKERR ISCreateGeneral(m_field_ptr->get_comm(), vec_is_lo.size(),
    //                            &*vec_is_lo.begin(), PETSC_COPY_VALUES,
    //                            &is_lo);
    //     CHKERR ISCreateGeneral(m_field_ptr->get_comm(), vec_is_hi.size(),
    //                            &*vec_is_hi.begin(), PETSC_COPY_VALUES,
    //                            &is_hi);

    //     CHKERR PCFieldSplitSetIS(pc, NULL, is_lo);
    //     CHKERR PCFieldSplitSetIS(pc, NULL, is_hi);

    //     PetscInt nsplits;
    //     KSP *sub_ksp;
    //     PC pc_lo;
    //     PC pc_hi;

    //     CHKERR PCFieldSplitGetSubKSP(pc, &nsplits, &sub_ksp);

    //     CHKERR KSPGetPC(sub_ksp[0], &pc_lo);
    //     CHKERR KSPGetPC(sub_ksp[1], &pc_hi);

    //     CHKERR PCSetType(pc_lo, PCLU);
    //     CHKERR PCFactorSetMatSolverPackage(pc_lo, MATSOLVERMUMPS);

    //     CHKERR PCSetType(pc_hi, PCJACOBI);

    //     CHKERR PetscFree(sub_ksp);
    //     CHKERR ISDestroy(&is_hi);
    //     CHKERR ISDestroy(&is_lo);
    //   }

    //   MoFEMFunctionReturn(0);
    // };

    // auto reset_fun = [](TS ts) -> PetscErrorCode {
    //   MoFEMFunctionBegin;
    //   // CHKERR TSReset(ts);
    //   SNES snes;
    //   CHKERR TSGetSNES(ts, &snes);
    //   KSP ksp;

    //   CHKERR SNESGetKSP(snes, &ksp);
    //   // CHKERR KSPReset(ksp);
    //   // CHKERR KSPSetFromOptions(ksp);
    //   PC pc;
    //   CHKERR KSPGetPC(ksp, &pc);
    //   PetscBool is_pcfs = PETSC_FALSE;
    //   PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);
    //   if(is_pcfs){
    //      CHKERR PCReset(pc);
    //     //  CHKERR PCSetFromOptions(pc);
    //   }
    //   MoFEMFunctionReturn(0);
    // };

    // CHKERR TSSetPostEvaluate(ts, reset_fun);
    // CHKERR TSSetPreStep(ts, field_split_fun);

    // auto ij_jacobian = [](TS ts, PetscReal t, Vec u, Vec u_t, PetscReal a,
    // Mat A,
    //                       Mat B, void *ctx) -> PetscErrorCode {
    //   MoFEMFunctionBegin;
    //   CHKERR TsSetIJacobian(ts, t, u, u_t, a, A, B, ctx);
    //   TsCtx *ts_ctx = static_cast<TsCtx *>(ctx);
    //   auto &m_field = ts_ctx->mField;

    //   auto field_split_fun = [&]() -> PetscErrorCode {
    //     MoFEMFunctionBegin;
    //     DM dm;
    //     CHKERR TSGetDM(ts, &dm);

    //     SNES snes;
    //     CHKERR TSGetSNES(ts, &snes);
    //     KSP ksp;
    //     CHKERR SNESGetKSP(snes, &ksp);
    //     PC pc;
    //     CHKERR KSPGetPC(ksp, &pc);
    //     PetscBool is_pcfs = PETSC_FALSE;
    //     PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);

    //     if (is_pcfs) {

    //       IS is_lo;
    //       const MoFEM::Problem *problem_ptr;
    //       CHKERR DMMoFEMGetProblemPtr(dm, &problem_ptr);
    //       auto dofs = problem_ptr->getNumeredRowDofs();
    //       auto nb_local_dofs = problem_ptr->getNbLocalDofsRow();
    //       Tag tag;
    //       CHKERR m_field.get_moab().tag_get_handle("_ORDER", tag);

    //       std::vector<int> vec_is_lo;

    //       vec_is_lo.reserve(dofs->size());

    //       for (auto &dof : *(dofs)) {
    //         auto loc_idx = dof->getPetscLocalDofIdx();

    //         if (loc_idx < nb_local_dofs) {
    //           auto ent = dof->getEnt();
    //           int tag_order;
    //           CHKERR m_field.get_moab().tag_get_data(tag, &ent, 1,
    //                                                       &tag_order);
    //           auto o = dof->getDofOrder();

    //           bool correct_order = false;

    //           if (dof->getSpace() == L2) {

    //             correct_order = (o < tag_order);
    //           } else {

    //             correct_order = (o <= tag_order);
    //           }

    //           if (correct_order) {
    //             vec_is_lo.push_back(dof->getPetscGlobalDofIdx());

    //           }
    //         }
    //       }
    //       std::sort(vec_is_lo.begin(), vec_is_lo.end());
    //       CHKERR ISCreateGeneral(m_field.get_comm(), vec_is_lo.size(),
    //                              &*vec_is_lo.begin(), PETSC_COPY_VALUES,
    //                              &is_lo);
    //       // CHKERR ISView(is_lo, PETSC_VIEWER_STDOUT_SELF);
    //       CHKERR PCReset(pc);
    //       CHKERR PCSetOperators(pc, A, B);
    //       CHKERR PCSetFromOptions(pc);
    //       // CHKERR KSPReset(ksp);
    //       CHKERR PCFieldSplitSetIS(pc, NULL, is_lo);

    //       CHKERR PCSetUp(pc);
    //       // CHKERR PCSetFromOptions(pc);

    //       // CHKERR KSPSetFromOptions(ksp);
    //       // CHKERR PCReset(pc);

    //       CHKERR ISDestroy(&is_lo);
    //     }
    //     MoFEMFunctionReturn(0);
    //   };
    //   CHKERR field_split_fun();
    //   // And rest the same like in PreProc
    //   MoFEMFunctionReturn(0);
    // };
    // then you do
    // MoFEM::TsCtx *ts_ctx;
    // DMMoFEMGetTsCtx(dm, &ts_ctx);
    // CHKERR DMTSSetIJacobian(dm, ij_jacobian, ts_ctx);

    CHKERR TSSolve(ts, X);
MoFEMFunctionReturn(0);
}

MoFEMErrorCode RDProblem::run_analysis(int nb_sp) {
  MoFEMFunctionBegin;

  global_error0 = 1;
  global_error1 = 1;
  post_error = 0;

  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &g_max_order, PETSC_NULL);
  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-min_order", &min_order, PETSC_NULL);
  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-roghness", &roghness, PETSC_NULL);

  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-adaptive", &adaptive, PETSC_NULL);

  // set nb_species
  CHKERR setup_system(); // only once
  nb_species = nb_sp;
  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR add_fe("MASS1", "FLUX1"); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR add_fe("MASS2", "FLUX2");
      if (nb_species == 3) {
        CHKERR add_fe("MASS3", "FLUX3");
      }
    }
  }
  CHKERR simple_interface->addDataField("ERROR", L2, AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simple_interface->addDataField("ERROR2", L2, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simple_interface->setFieldOrder("ERROR", 0); // approximation order for error
  CHKERR simple_interface->setFieldOrder("ERROR2", 0);

  CHKERR simple_interface->setUp();

    auto creat_tags = [&]() {
    MoFEMFunctionBegin;
    int o_rder = 1;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-tag_order", &o_rder, PETSC_NULL);
    CHKERR m_field.get_moab().tag_get_handle(
        "_ORDER", 1, MB_TYPE_INTEGER, thMaxOrder, MB_TAG_CREAT | MB_TAG_DENSE, &o_rder);

    int r_ule = 1;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-tag_rule", &r_ule, PETSC_NULL);
    CHKERR m_field.get_moab().tag_get_handle(
        "_RULE", 1, MB_TYPE_INTEGER, thQuadRule, MB_TAG_CREAT | MB_TAG_DENSE, &r_ule);
    MoFEMFunctionReturn(0);
  };

  CHKERR creat_tags();

  CHKERR set_blockData(material_blocks);

  CHKERR extract_bd_ents("ESSENTIAL", "NATURAL", "INTERNAL"); // nb_species times

  CHKERR extract_initial_ents(2, inner_surface1);
  CHKERR extract_initial_ents(3, inner_surface2);
  CHKERR extract_initial_ents(4, inner_surface3);

  Range init_surf;

  init_surf.merge(inner_surface1);
  init_surf.merge(inner_surface2);
  init_surf.merge(inner_surface3);

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    // CHKERR extract_initial_ents(2, inner_surface1);
    CHKERR update_slow_rhs("MASS1", mass_values_ptr1);
    if (nb_species == 1) {
      // vol_ele_slow_rhs->getOpPtrVector().push_back(new OpComputeSlowValue(
      //     "MASS1", data1, data1, data1, material_blocks));
    } else if (nb_species == 2 || nb_species == 3) {
      // CHKERR extract_initial_ents(3, inner_surface2);
      CHKERR update_slow_rhs("MASS2", mass_values_ptr2);
      if (nb_species == 2) {
        // vol_ele_slow_rhs->getOpPtrVector().push_back(new OpComputeSlowValue(
        //     "MASS1", data1, data2, data2, material_blocks));
      } else if (nb_species == 3) {
        // CHKERR extract_initial_ents(4, inner_surface3);
        CHKERR update_slow_rhs("MASS3", mass_values_ptr3);
        // vol_ele_slow_rhs->getOpPtrVector().push_back(new OpComputeSlowValue(
        //     "MASS1", data1, data2, data3, material_blocks));
      }
    }
  }
  natural_bdry_ele_slow_rhs->getOpPtrVector().push_back(
      new OpSetContrariantPiolaTransformOnEdge());
  // put 0 if it is not reaction diffusion
  if ((nb_species == 1 || nb_species == 2 || nb_species == 3)) {
    // CHKERR push_slow_rhs("MASS1", "FLUX1", data1); // nb_species times
    vol_ele_slow_rhs->getOpPtrVector().push_back(
      new OpAssembleSlowRhsV("MASS1",
                             material_blocks,
                             data1,
                             data2,
                             data3, 
                             ExactFunction(), 
                             ExactFunctionDot(), 
                             ExactFunctionLap(),
                             m_field));
    if (nb_species == 2 || nb_species == 3) {
      // CHKERR push_slow_rhs("MASS2", "FLUX2", data2);
      vol_ele_slow_rhs->getOpPtrVector().push_back(
      new OpAssembleSlowRhsV("MASS2",
                             material_blocks,
                             data1,
                             data2,
                             data3, 
                             ExactFunction(), 
                             ExactFunctionDot(), 
                             ExactFunctionLap(),
                             m_field));
      if (nb_species == 3) {
        // CHKERR push_slow_rhs("MASS3", "FLUX3", data3);
        vol_ele_slow_rhs->getOpPtrVector().push_back(
      new OpAssembleSlowRhsV("MASS3",
                             material_blocks,
                             data1,
                             data2,
                             data3, 
                             ExactFunction(), 
                             ExactFunctionDot(), 
                             ExactFunctionLap(),
                             m_field));
      }
    }
  }

  CHKERR update_vol_fe(vol_ele_stiff_rhs, data1);

  // boost::shared_ptr<MatrixDouble> mass_grad_ptr1 =
  //     boost::shared_ptr<MatrixDouble>(data1, &data1->mass_grads);

  // boost::shared_ptr<MatrixDouble> mass_grad_ptr2 =
  //     boost::shared_ptr<MatrixDouble>(data2, &data2->mass_grads);

  // boost::shared_ptr<MatrixDouble> mass_grad_ptr3 =
  //     boost::shared_ptr<MatrixDouble>(data3, &data3->mass_grads);

  // vol_ele_stiff_rhs->getOpPtrVector().push_back(
  //     new OpSetInvJacH1ForFace(data1->inv_jac));

  // vol_ele_stiff_rhs->getOpPtrVector().push_back(
  //     new OpCalculateScalarFieldGradient<2>("MASS1", mass_grad_ptr1, MBTRI));

  

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR update_stiff_rhs("MASS1", "FLUX1", mass_values_ptr1,
                            flux_values_ptr1, mass_dots_ptr1, flux_divs_ptr1);

    CHKERR push_stiff_rhs("MASS1", "FLUX1", data1,
                          material_blocks); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR update_stiff_rhs("MASS2", "FLUX2", mass_values_ptr2,
                              flux_values_ptr2, mass_dots_ptr2, flux_divs_ptr2);
      CHKERR push_stiff_rhs("MASS2", "FLUX2", data2, material_blocks);
      if (nb_species == 3) {
        CHKERR update_stiff_rhs("MASS3", "FLUX3", mass_values_ptr3,
                                flux_values_ptr3, mass_dots_ptr3,
                                flux_divs_ptr3);
        CHKERR push_stiff_rhs("MASS3", "FLUX3", data3, material_blocks);
      }
    }
  }

  CHKERR update_vol_fe(vol_ele_stiff_lhs, data1);

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR update_stiff_lhs("MASS1", "FLUX1", mass_values_ptr1,
                            flux_values_ptr1);
    CHKERR push_stiff_lhs("MASS1", "FLUX1", data1,
                          material_blocks); // nb_species times
    vol_ele_stiff_rhs->getOpPtrVector().push_back(
        new OpCalculateGradL2<2>("MASS1", data1->inv_jac, data1->mass_grads));
    vol_ele_stiff_rhs->getOpPtrVector().push_back(new OpPostError(
        "ERROR2", "MASS1", ExactFunctionDot(), ExactFunctionLap(), data1, data1, data2, data3,
        material_blocks, post_error, m_field));
    if (nb_species == 2 || nb_species == 3) {
      CHKERR update_stiff_lhs("MASS2", "FLUX2", mass_values_ptr2,
                              flux_values_ptr2);
      CHKERR push_stiff_lhs("MASS2", "FLUX2", data2, material_blocks);
      vol_ele_stiff_rhs->getOpPtrVector().push_back(
          new OpCalculateGradL2<2>("MASS2", data1->inv_jac, data2->mass_grads));
      vol_ele_stiff_rhs->getOpPtrVector().push_back(new OpPostError(
          "ERROR2", "MASS2", ExactFunctionDot(), ExactFunctionLap(),data2, data1, data2, data3,
          material_blocks, post_error, m_field));
      if (nb_species == 3) {
        CHKERR update_stiff_lhs("MASS3", "FLUX3", mass_values_ptr3,
                                flux_values_ptr3);
        CHKERR push_stiff_lhs("MASS3", "FLUX3", data3, material_blocks);
        vol_ele_stiff_rhs->getOpPtrVector().push_back(new OpCalculateGradL2<2>(
            "MASS1", data1->inv_jac, data3->mass_grads));

        vol_ele_stiff_rhs->getOpPtrVector().push_back(
            new OpPostError("ERROR2", "MASS3", ExactFunctionDot(), ExactFunctionLap(), data3,
                            data1, data2, data3, material_blocks, post_error, m_field));
      }
    }
  }



  

  


  // vol_ele_stiff_rhs->getOpPtrVector().push_back(
  //     new OpError(ExactFunction(), ExactFunctionLap(), ExactFunctionGrad(),
  //     data1, material_blocks, global_error0, global_error1, m_field));
  

  JumpData jump_data;

  // skeleton_fe->getOpPtrVector().push_back(
  //     new OpZeroDof("FLUX1", "ERROR2", m_field));
  skeleton_fe->getOpPtrVector().push_back(
      new OpSetContrariantPiolaTransformOnEdge());

  // skeleton_fe->getOpPtrVector().push_back(new OpCalculateJumpErrors(
  //     "FLUX1", "MASS1", "ERROR2", m_field, jump_data, post_error));

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    skeleton_fe->getOpPtrVector().push_back(new OpCalculateJumpErrors(
        "FLUX1", "MASS1", "ERROR2", m_field, jump_data, post_error));
    if (nb_species == 2 || nb_species == 3) {
      skeleton_fe->getOpPtrVector().push_back(new OpCalculateJumpErrors(
          "FLUX2", "MASS2", "ERROR2", m_field, jump_data, post_error));
      if (nb_species == 3) {
        skeleton_fe->getOpPtrVector().push_back(new OpCalculateJumpErrors(
            "FLUX3", "MASS3", "ERROR2", m_field, jump_data, post_error));
      }
    }
  }

  CHKERR set_integration_rule();

  dm = simple_interface->getDM();
  ts = createTS(m_field.get_comm());
  boost::shared_ptr<FaceEle> initial_mass_ele(new FaceEle(m_field));

  auto initial_mass_ele_rule = [&](int, int, int) -> int {
    auto fe_ent = initial_mass_ele->getFEEntityHandle();
    int quad_rule;
    CHKERR m_field.get_moab().tag_get_data(thQuadRule, &fe_ent, 1,
                                           &quad_rule);

    return 2 * (quad_rule + 0);
  };
  initial_mass_ele->getRuleHook = initial_mass_ele_rule;

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR apply_IC("MASS1", init_surf, data1,
                    initial_mass_ele); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR apply_IC("MASS2", init_surf, data1, initial_mass_ele);
      if (nb_species == 3) {
        CHKERR apply_IC("MASS3", init_surf, data1, initial_mass_ele);
      }
    }
  }
  CHKERR DMoFEMLoopFiniteElements(dm, simple_interface->getDomainFEName(),
                                  initial_mass_ele);

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    CHKERR apply_BC("FLUX1"); // nb_species times
    if (nb_species == 2 || nb_species == 3) {
      CHKERR apply_BC("FLUX2");
      if (nb_species == 3) {
        CHKERR apply_BC("FLUX3");
      }
    }
  }



  CHKERR loop_fe();                          // only once
  post_proc->generateReferenceElementMesh(); // only once

  post_proc->getOpPtrVector().push_back(new OpCalculateJacForFace(data1->jac));
  // post_proc->getOpPtrVector().push_back(
  //     new OpCalculateInvJacForFace(data1->inv_jac));
  // post_proc->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());

  // post_proc->getOpPtrVector().push_back(
  //     new OpSetContravariantPiolaTransformFace(data1->jac));

  // post_proc->getOpPtrVector().push_back(
  //     new OpSetInvJacHcurlFace(data1->inv_jac));

  

  // post_proc->getOpPtrVector().push_back(
  //     new OpCalculateHdivVectorField<3>("FLUX1", flux_values_ptr1));

  if (nb_species == 1 || nb_species == 2 || nb_species == 3) {
    // CHKERR post_proc_fields("MASS1", "FLUX1");
    // post_proc->getOpPtrVector().push_back(
    //     new OpCalculateScalarFieldValues("MASS1", mass_values_ptr1, MBTRI));
    post_proc->addFieldValuesPostProc("MASS1");
    // post_proc->addFieldValuesPostProc("FLUX1");
    if (nb_species == 2 || nb_species == 3) {
      // post_proc->getOpPtrVector().push_back(
      //     new OpCalculateScalarFieldValues("MASS1", mass_values_ptr1, MBTRI));
      post_proc->addFieldValuesPostProc("MASS2");
      // CHKERR post_proc_fields("MASS2", "FLUX2");
      if (nb_species == 3) {
        // post_proc->getOpPtrVector().push_back(
        //     new OpCalculateScalarFieldValues("MASS1", mass_values_ptr1, MBTRI));
        post_proc->addFieldValuesPostProc("MASS3");
        // CHKERR post_proc_fields("MASS3", "FLUX3");
      }
    }
  }
  
  post_proc->addFieldValuesPostProc("ERROR2");
  post_proc->addFieldValuesPostProc("ERROR");

   maxError = 0;
  max_post_error_fe->getOpPtrVector().push_back(new OpMaxError("ERROR2", maxError));
  monitor_ptr = boost::shared_ptr<Monitor>(
      new Monitor(cOmm, rAnk, dm, ts, max_post_error_fe, post_proc, global_error0,
                  global_error1, post_error, maxError, m_field)); // nb_species times
  CHKERR output_result();          // only once
  CHKERR solve();                  // only once
  MoFEMFunctionReturn(0);
}

int main(int argc, char *argv[]) {
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    
    int nb_species = 1;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-nb_species",
                              &nb_species, PETSC_NULL);
    RDProblem reac_diff_problem(core);
    CHKERR reac_diff_problem.run_analysis(nb_species);
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}