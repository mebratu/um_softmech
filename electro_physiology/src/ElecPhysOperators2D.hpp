#ifndef __ELECPHYSOPERATORS2D_HPP__
#define __ELECPHYSOPERATORS2D_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace ElectroPhysiology {

using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
    FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
    FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
    EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using OpFaceEle = FaceEle::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using PostProc = PostProcFaceOnRefinedMesh;

double factor = 1.0/ 12.9;

const double B = 0.0;
const double B_epsilon = 0.0;

int save_every_nth_step = 16;

const double essen_value = 0;

FTensor::Index<'i', 3> i;

// problem parameters
const double alpha = 0.01;
const double gma = 0.002;
const double b = 0.15;
const double c = 8.00;
const double mu1 = 0.20;
const double mu2 = 0.30;

auto get_nb_dofs = [](EntData &data, Tag th_order, MoFEM::Interface &m_field) {
  if (data.getFieldDofs().empty())
    return 0;
  else {
    auto fe_ent = data.getFieldDofs()[0]->getEnt();
    int order;
    CHKERR m_field.get_moab().tag_get_data(th_order, &fe_ent, 1, &order);
    if (data.getFieldDofs()[0]->getSpace() == L2)
      return data.getFieldDofs()[0]->getOrderNbDofs(order - 1);
    else
      return data.getFieldDofs()[0]->getOrderNbDofs(order);
  }
};

struct BlockEPData {
  int block_id;

  Range block_ents;

  double B0; // species mobility

  BlockEPData()
      : B0(0.2) {}
};

BlockEPData block;

const double D_tilde = 1e-2;

struct PreviousData {
  MatrixDouble flux_values;
  VectorDouble flux_divs;

  VectorDouble mass_dots;
  VectorDouble mass_values;
  MatrixDouble mass_grads;

  MatrixDouble jac;
  MatrixDouble inv_jac;

  PreviousData() {
    jac.resize(2, 2, false);
    inv_jac.resize(2, 2, false);
  }
};
struct OpEssentialBC : public OpEdgeEle {
  OpEssentialBC(const std::string &flux_field, 
                Range             &essential_bd_ents)
      : OpEdgeEle(flux_field, OpEdgeEle::OPROW)
      , essential_bd_ents(essential_bd_ents) {}

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      bool is_essential =
          (essential_bd_ents.find(fe_ent) != essential_bd_ents.end());
      if (is_essential) {
        int nb_gauss_pts = getGaussPts().size2();
        int size2 = data.getN().size2();
        if (3 * nb_dofs != static_cast<int>(data.getN().size2()))
          SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
                  "wrong number of dofs");
        nN.resize(nb_dofs, nb_dofs, false);
        nF.resize(nb_dofs, false);
        nN.clear();
        nF.clear();

        auto t_row_tau = data.getFTensor1N<3>();

        auto dir = getDirection();
        double len = sqrt(dir[0] * dir[0] + dir[1] * dir[1] + dir[2] * dir[2]);

        FTensor::Tensor1<double, 3> t_normal(-dir[1] / len, dir[0] / len,
                                             dir[2] / len);

        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();

        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          const double a = t_w * vol;
          for (int rr = 0; rr != nb_dofs; rr++) {
            auto t_col_tau = data.getFTensor1N<3>(gg, 0);
            nF[rr] += a * essen_value * t_row_tau(i) * t_normal(i);
            for (int cc = 0; cc != nb_dofs; cc++) {
              nN(rr, cc) += a * (t_row_tau(i) * t_normal(i)) *
                            (t_col_tau(i) * t_normal(i));
              ++t_col_tau;
            }
            ++t_row_tau;
          }
          ++t_w;
        }

        cholesky_decompose(nN);
        cholesky_solve(nN, nF, ublas::lower());

        for (auto &dof : data.getFieldDofs()) {
          dof->getFieldData() = nF[dof->getEntDofIdx()];
        }
      }
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble nN;
  VectorDouble nF;
  Range &essential_bd_ents;
};

// Assembly of system mass matrix
// //***********************************************

// Mass matrix corresponding to the flux equation.
// 01. Note that it is an identity matrix
struct OpInitialMass : public OpFaceEle {
  OpInitialMass(const std::string &mass_field, 
                Range             &inner_surface, 
                double            &init_val,
                MoFEM::Interface  &m_field)
      : OpFaceEle(mass_field, OpFaceEle::OPROW)
      , innerSurface(inner_surface)
      , initVal(init_val)
      , mField(m_field) {}

  MoFEM::Interface &mField;

  MatrixDouble nN;
  VectorDouble nF;
  double &initVal;
  Range &innerSurface;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nn_dofs = data.getFieldData().size();
    if (nn_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      bool is_inner_side = (innerSurface.find(fe_ent) != innerSurface.end());
      if (is_inner_side) {
        const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);
        int nb_gauss_pts = getGaussPts().size2();

        nN.resize(nn_dofs, nn_dofs, false);
        nF.resize(nn_dofs, false);
        nN.clear();
        nF.clear();

        int ii = nb_dofs;
        for (; ii < nn_dofs; ++ii)
          nN(ii, ii) = 1.0;

        
        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();

        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          auto t_row_mass = data.getFTensor0N(gg, 0);
          const double a = t_w * vol;
          double r = initVal;
          for (int rr = 0; rr != nb_dofs; rr++) {
            auto t_col_mass = data.getFTensor0N(gg, 0);
            nF[rr] += a * r * t_row_mass;
            for (int cc = 0; cc != nb_dofs; cc++) {
              nN(rr, cc) += a * t_row_mass * t_col_mass;
              ++t_col_mass;
            }
            ++t_row_mass;
          }
          ++t_w;
        }

        cholesky_decompose(nN);
        cholesky_solve(nN, nF, ublas::lower());

        for (auto &dof : data.getFieldDofs()) {
          dof->getFieldData() = nF[dof->getEntDofIdx()];

          // this is only to check
          // data.getFieldData()[dof->getEntDofIdx()] = nF[dof->getEntDofIdx()];
        }
      }
    }
    MoFEMFunctionReturn(0);
  }
};

struct OpSolveRecovery : public OpFaceEle {
  typedef boost::function<double(const double, const double, const double)>
      Method;
  OpSolveRecovery(const std::string               &mass_field,
                  boost::shared_ptr<PreviousData> &data_u,
                  boost::shared_ptr<PreviousData> &data_v, 
                  Method                          runge_kutta4,
                  MoFEM::Interface                &m_field)
      : OpFaceEle(mass_field, OpFaceEle::OPROW)
      , dataU(data_u)
      , dataV(data_v)
      , rungeKutta4(runge_kutta4)
      , mField(m_field) {}
  boost::shared_ptr<PreviousData> dataU;
  boost::shared_ptr<PreviousData> dataV;
  Method rungeKutta4;

  MoFEM::Interface &mField;

  MatrixDouble nN;
  VectorDouble nF;
  double initVal;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nn_dofs = data.getFieldData().size();
    if (nn_dofs) {
      int nb_gauss_pts = getGaussPts().size2();
      const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);

      nN.resize(nn_dofs, nn_dofs, false);
      nF.resize(nn_dofs, false);
      nN.clear();
      nF.clear();

      int ii = nb_dofs;
      for (; ii < nn_dofs; ++ii)
        nN(ii, ii) = 1.0;

      auto t_val_u = getFTensor0FromVec(dataU->mass_values);
      auto t_val_v = getFTensor0FromVec(dataV->mass_values);

      double dt;
      CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);

      
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg < nb_gauss_pts; gg++) {
        auto t_row_mass = data.getFTensor0N(gg, 0);
        const double a = t_w * vol;
        const double vn = rungeKutta4(t_val_u, t_val_v, dt);
        for (int rr = 0; rr != nb_dofs; rr++) {
          auto t_col_mass = data.getFTensor0N(gg, 0);
          nF[rr] += a * vn * t_row_mass;
          for (int cc = 0; cc != nb_dofs; cc++) {
            nN(rr, cc) += a * t_row_mass * t_col_mass;
            ++t_col_mass;
          }
          ++t_row_mass;
        }
        ++t_w;
        ++t_val_u;
        ++t_val_v;
      }

      cholesky_decompose(nN);
      cholesky_solve(nN, nF, ublas::lower());

      for (auto &dof : data.getFieldDofs()) {
        dof->getFieldData() = nF[dof->getEntDofIdx()];

        // this is only to check
        // data.getFieldData()[dof->getEntDofIdx()] = nF[dof->getEntDofIdx()];
      }
    }
    MoFEMFunctionReturn(0);
  }
};

// Assembly of RHS for explicit (slow)
// part//**************************************

// 2. RHS for explicit part of the mass balance equation
struct OpAssembleSlowRhsV : OpFaceEle // R_V
{
  typedef boost::function<double(const double, const double)>
      FUVal;
  OpAssembleSlowRhsV(std::string                     mass_field,
                     boost::shared_ptr<PreviousData> &common_datau,
                     boost::shared_ptr<PreviousData> &common_datav, 
                     FUVal                           rhs_u,
                     MoFEM::Interface                &m_field)
      : OpFaceEle(mass_field, OpFaceEle::OPROW)
      , commonDatau(common_datau)
      , commonDatav(common_datav)
      , rhsU(rhs_u)
      , mField(m_field) {}
 
  MoFEM::Interface    &mField;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nn_dofs = data.getIndices().size();
    if (nn_dofs) {
      const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField); 

      vecF.resize(nn_dofs, false);
      mat.resize(nn_dofs, nn_dofs, false);
      vecF.clear();
      mat.clear();

      int ii = nb_dofs;
      for (; ii < nn_dofs; ++ii)
        mat(ii, ii) = 1.0;

      const int nb_integration_pts = getGaussPts().size2();
      auto t_u_value = getFTensor0FromVec(commonDatau->mass_values);
      auto t_v_value = getFTensor0FromVec(commonDatav->mass_values);
      
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      // const double ct = getFEMethod()->ts_t - 0.01;
      // auto t_coords = getFTensor1CoordsAtGaussPts();
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_v_base = data.getFTensor0N(gg, 0);
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_dofs; ++rr) {
          double rhs = rhsU(t_u_value, t_v_value);

          auto t_col_v_base = data.getFTensor0N(gg, 0);
          vecF[rr] += a * rhs * t_row_v_base;

          for (int cc = 0; cc != nb_dofs; ++cc) {
            mat(rr, cc) += a * t_row_v_base * t_col_v_base;
            ++t_col_v_base;
          }
          ++t_row_v_base;
        }
        ++t_u_value;
        ++t_v_value;
        ++t_w;
        // ++t_coords;
      }
      cholesky_decompose(mat);
      cholesky_solve(mat, vecF, ublas::lower());

      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonDatau;
  boost::shared_ptr<PreviousData> commonDatav;
  VectorDouble vecF;
  MatrixDouble mat;
  FUVal rhsU;


  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
  FTensor::Number<2> NZ;
};

// // 5. RHS contribution of the natural boundary condition
// struct OpAssembleNaturalBCRhsTau : OpFaceEle // R_tau_2
// {
//   OpAssembleNaturalBCRhsTau(std::string flux_field, Range &natural_bd_ents)
//       : OpFaceEle(flux_field, OpFaceEle::OPROW),
//         natural_bd_ents(natural_bd_ents) {}

//   MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
//     MoFEMFunctionBegin;
//     const int nb_dofs = data.getIndices().size();

//     if (nb_dofs) {
//       EntityHandle row_side_ent = data.getFieldDofs()[0]->getEnt();

//       bool is_natural =
//           (natural_bd_ents.find(row_side_ent) != natural_bd_ents.end());
//       if (is_natural) {
//         // cerr << "In NaturalBCRhsTau..." << endl;
//         vecF.resize(nb_dofs, false);
//         vecF.clear();
//         const int nb_integration_pts = getGaussPts().size2();
//         auto t_tau_base = data.getFTensor1N<3>();

//         auto dir = getDirection();
//         FTensor::Tensor1<double, 3> t_normal(-dir[1], dir[0], dir[2]);

//         auto t_w = getFTensor0IntegrationWeight();

//         for (int gg = 0; gg != nb_integration_pts; ++gg) {
//           const double a = t_w;
//           for (int rr = 0; rr != nb_dofs; ++rr) {
//             vecF[rr] += (t_tau_base(i) * t_normal(i) * natu_value) * a;
//             ++t_tau_base;
//           }
//           ++t_w;
//         }
//         CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
//                             PETSC_TRUE);
//         CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
//                             ADD_VALUES);
//       }
//     }
//     MoFEMFunctionReturn(0);
//   }

// private:
//   VectorDouble vecF;
//   Range natural_bd_ents;
// };

// Assembly of RHS for the implicit (stiff) part excluding the essential
// boundary //**********************************
// 3. Assembly of F_tau excluding the essential boundary condition
template <int dim>
struct OpAssembleStiffRhsTau : OpFaceEle //  F_tau_1
{
  OpAssembleStiffRhsTau(std::string                     flux_field,
                        boost::shared_ptr<PreviousData> &data,
                        std::map<int, BlockEPData>        &block_map,
                        MoFEM::Interface                &m_field)
      : OpFaceEle(flux_field, OpFaceEle::OPROW)
      , commonData(data)
      , setOfBlock(block_map)
      , mField(m_field) {}

  MoFEM::Interface &mField;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;

    const int nn_dofs = data.getIndices().size();
    if (nn_dofs) {

      const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);

      vecF.resize(nn_dofs, false);
      vecF.clear();

      const int nb_integration_pts = getGaussPts().size2();
      auto t_flux_value = getFTensor1FromMat<3>(commonData->flux_values);
      auto t_mass_value = getFTensor0FromVec(commonData->mass_values);
      

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg < nb_integration_pts; ++gg) {

        auto t_tau_base = data.getFTensor1N<3>(gg, 0);

        auto t_tau_grad = data.getFTensor2DiffN<3, 2>(gg, 0);

        const double K = B_epsilon + (block.B0 + B * t_mass_value);
        const double K_inv = 1. / K;
        const double a = vol * t_w;
        for (int rr = 0; rr < nb_dofs; ++rr) {
          double div_base = t_tau_grad(0, 0) + t_tau_grad(1, 1);
          vecF[rr] += (K_inv * t_tau_base(i) * t_flux_value(i) -
                       div_base * t_mass_value) *
                      a;
          ++t_tau_base;
          ++t_tau_grad;
        }
        ++t_flux_value;
        ++t_mass_value;
        ++t_w;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  VectorDouble vecF;
  std::map<int, BlockEPData> setOfBlock;
};
// 4. Assembly of F_v
template <int dim>
struct OpAssembleStiffRhsV : OpFaceEle // F_V
{
  typedef boost::function<double(const double, const double)>
      FUval;
  OpAssembleStiffRhsV(std::string                     flux_field,
                      boost::shared_ptr<PreviousData> &datau,
                      boost::shared_ptr<PreviousData> &datav, 
                      FUval                           rhs_u,
                      std::map<int, BlockEPData>        &block_map, 
                      Range                           &stim_region,
                      MoFEM::Interface                &m_field)
      : OpFaceEle(flux_field, OpFaceEle::OPROW)
      , commonDatau(datau)
      , commonDatav(datav)
      , setOfBlock(block_map)
      , rhsU(rhs_u)
      , stimRegion(stim_region)
      , mField(m_field) {}

  MoFEM::Interface &mField;
  Range            &stimRegion;
  FUval            rhsU;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nn_dofs = data.getIndices().size();
    // cerr << "In StiffRhsV ..." << endl;
    if (nn_dofs) {
      const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);

      // auto find_block_data = [&]() {
      //   EntityHandle fe_ent = getFEEntityHandle();
      //   BlockEPData *block_raw_ptr = nullptr;
      //   for (auto &m : setOfBlock) {
      //     if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
      //       block_raw_ptr = &m.second;
      //       break;
      //     }
      //   }
      //   return block_raw_ptr;
      // };

      // auto block_data_ptr = find_block_data();
      // if (!block_data_ptr)
      //   SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      // auto &block_data = *block_data_ptr;

      vecF.resize(nn_dofs, false);
      vecF.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_u_value = getFTensor0FromVec(commonDatau->mass_values);
      auto t_v_value = getFTensor0FromVec(commonDatav->mass_values);

      auto t_mass_dot = getFTensor0FromVec(commonDatau->mass_dots);
      auto t_flux_div = getFTensor0FromVec(commonDatau->flux_divs);
      
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();
      auto t_coords = getFTensor1CoordsAtGaussPts();

      const double c_time = getFEMethod()->ts_t;

      double dt;
      CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);

      double stim = 0.0;

      double T = time_stimulus;
      double duration = duration_stimulus;

      if (T - dt < c_time && c_time <= T + duration) {
        EntityHandle stim_ent = getFEEntityHandle();
        if (stimRegion.find(stim_ent) != stimRegion.end()) {
          stim = 40.0;
        } else {
          stim = 0.0;
        }
      }
      for (int gg = 0; gg < nb_integration_pts; ++gg) {

        auto t_row_v_base = data.getFTensor0N(gg, 0);

        const double a = vol * t_w;

        double rhsu = rhsU(t_u_value, t_v_value);

        for (int rr = 0; rr < nb_dofs; ++rr) {
          vecF[rr] += (t_row_v_base * (t_mass_dot + t_flux_div - 0*rhsu - factor * stim)) * a;
          ++t_row_v_base;
        }
        ++t_mass_dot;
        ++t_flux_div;
        ++t_u_value;
        ++t_v_value;
        ++t_w;
        ++t_coords;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonDatau;
  boost::shared_ptr<PreviousData> commonDatav;
  VectorDouble vecF;
  std::map<int, BlockEPData> setOfBlock;

  // FVal exactValue;
  // FVal exactDot;
  // FVal exactLap;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
};

// Tangent operator
// //**********************************************
// 7. Tangent assembly for F_tautau excluding the essential boundary condition
template <int dim>
struct OpAssembleLhsTauTau : OpFaceEle // A_TauTau_1
{
  OpAssembleLhsTauTau(std::string                     flux_field,
                      boost::shared_ptr<PreviousData> &commonData,
                      std::map<int, BlockEPData>        &block_map,
                      MoFEM::Interface                &m_field)
      : OpFaceEle(flux_field, flux_field, OpFaceEle::OPROWCOL)
      , setOfBlock(block_map)
      , commonData(commonData)
      , mField(m_field) {
    sYmm = false;
  }

  MoFEM::Interface &mField;

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nn_row_dofs = row_data.getIndices().size();
    const int nn_col_dofs = col_data.getIndices().size();

    if (nn_row_dofs && nn_col_dofs) {
      // auto find_block_data = [&]() {
      //   EntityHandle fe_ent = getFEEntityHandle();
      //   BlockEPData *block_raw_ptr = nullptr;
      //   for (auto &m : setOfBlock) {
      //     if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
      //       block_raw_ptr = &m.second;
      //       break;
      //     }
      //   }
      //   return block_raw_ptr;
      // };

      // auto block_data_ptr = find_block_data();
      // if (!block_data_ptr)
      //   SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      // auto &block_data = *block_data_ptr;

      const int nb_row_dofs = get_nb_dofs(row_data, thMaxOrder, mField);
      const int nb_col_dofs = get_nb_dofs(col_data, thMaxOrder, mField);

      mat.resize(nn_row_dofs, nn_col_dofs, false);
      mat.clear();

      const bool on_block_diag = row_side == col_side && row_type == col_type;
      if (on_block_diag) {
        int ii = nb_row_dofs;
        for (; ii < nn_row_dofs; ++ii)
          mat(ii, ii) = 1.0;
      }

      const int nb_integration_pts = getGaussPts().size2();
      auto t_mass_value = getFTensor0FromVec(commonData->mass_values);

      

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_tau_base = row_data.getFTensor1N<3>(gg, 0);
        const double a = vol * t_w;
        const double K = B_epsilon + (block.B0 + B * t_mass_value);
        const double K_inv = 1. / K;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_tau_base = col_data.getFTensor1N<3>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            mat(rr, cc) += (K_inv * t_row_tau_base(i) * t_col_tau_base(i)) * a;
            ++t_col_tau_base;
          }
          ++t_row_tau_base;
        }
        ++t_mass_value;
        ++t_w;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
      // if (row_side != col_side || row_type != col_type) {
      //   transMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(transMat) = trans(mat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &transMat(0, 0), ADD_VALUES);
      // }
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  MatrixDouble mat, transMat;
  Range essential_bd_ents;
  std::map<int, BlockEPData> setOfBlock;
};

// 9. Assembly of tangent for F_tau_v excluding the essential bc
template <int dim>
struct OpAssembleLhsTauV : OpFaceEle // E_TauV
{
  OpAssembleLhsTauV(std::string                     flux_field, 
                    std::string                     mass_field,
                    boost::shared_ptr<PreviousData> &data,
                    std::map<int, BlockEPData>        &block_map,
                    MoFEM::Interface                &m_field)
      : OpFaceEle(flux_field, mass_field, OpFaceEle::OPROWCOL)
      , commonData(data)
      , setOfBlock(block_map)
      , mField(m_field) {
    sYmm = false;
  }

  MoFEM::Interface   &mField;

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nn_row_dofs = row_data.getIndices().size();
    const int nn_col_dofs = col_data.getIndices().size();

    if (nn_row_dofs && nn_col_dofs) {
      // auto find_block_data = [&]() {
      //   EntityHandle fe_ent = getFEEntityHandle();
      //   BlockEPData *block_raw_ptr = nullptr;
      //   for (auto &m : setOfBlock) {
      //     if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
      //       block_raw_ptr = &m.second;
      //       break;
      //     }
      //   }
      //   return block_raw_ptr;
      // };

      // auto block_data_ptr = find_block_data();
      // if (!block_data_ptr)
      //   SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      // auto &block_data = *block_data_ptr;

      const int nb_row_dofs = get_nb_dofs(row_data, thMaxOrder, mField);
      const int nb_col_dofs = get_nb_dofs(col_data, thMaxOrder, mField);
      mat.resize(nn_row_dofs, nn_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();
     
      auto t_mass_value = getFTensor0FromVec(commonData->mass_values);
      auto t_flux_value = getFTensor1FromMat<3>(commonData->flux_values);
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_tau_base = row_data.getFTensor1N<3>(gg, 0);

        auto t_row_tau_grad = row_data.getFTensor2DiffN<3, 2>(gg, 0);
        const double a = vol * t_w;
        const double K = B_epsilon + (block.B0 + B * t_mass_value);
        const double K_inv = 1. / K;
        const double K_diff = B;

        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_v_base = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            double div_row_base = t_row_tau_grad(0, 0) + t_row_tau_grad(1, 1);
            mat(rr, cc) += (-(t_row_tau_base(i) * t_flux_value(i) * K_inv *
                              K_inv * K_diff * t_col_v_base) -
                            (div_row_base * t_col_v_base)) *
                           a;
            ++t_col_v_base;
          }
          ++t_row_tau_base;
          ++t_row_tau_grad;
        }
        ++t_w;
        ++t_mass_value;
        ++t_flux_value;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  MatrixDouble mat;
  std::map<int, BlockEPData> setOfBlock;
};

// 10. Assembly of tangent for F_v_tau
struct OpAssembleLhsVTau : OpFaceEle // C_VTau
{
  OpAssembleLhsVTau(std::string      mass_field, 
                    std::string      flux_field,
                    MoFEM::Interface &m_field)
      : OpFaceEle(mass_field, flux_field, OpFaceEle::OPROWCOL)
      , mField(m_field) {
    sYmm = false;
  }

  MoFEM::Interface &mField;
  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nn_row_dofs = row_data.getIndices().size();
    const int nn_col_dofs = col_data.getIndices().size();

    if (nn_row_dofs && nn_col_dofs) {
      const int nb_row_dofs = get_nb_dofs(row_data, thMaxOrder, mField);
      const int nb_col_dofs = get_nb_dofs(col_data, thMaxOrder, mField);

      mat.resize(nn_row_dofs, nn_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();
      
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_v_base = row_data.getFTensor0N(gg, 0);
        const double a = vol * t_w;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_tau_grad = col_data.getFTensor2DiffN<3, 2>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            double div_col_base = t_col_tau_grad(0, 0) + t_col_tau_grad(1, 1);
            mat(rr, cc) += (t_row_v_base * div_col_base) * a;
            ++t_col_tau_grad;
          }
          ++t_row_v_base;
        }
        ++t_w;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat;
};

// 11. Assembly of tangent for F_v_v
struct OpAssembleLhsVV : OpFaceEle // D
{
  typedef boost::function<double(const double, const double)> FUval;
  OpAssembleLhsVV(std::string                     mass_field,
                  boost::shared_ptr<PreviousData> &datau,
                  boost::shared_ptr<PreviousData> &datav,
                  FUval                           Drhs_u,
                  MoFEM::Interface                &m_field)
      : OpFaceEle(mass_field, mass_field, OpFaceEle::OPROWCOL) 
      , DRhs_u(Drhs_u)
      , dataU(datau)
      , dataV(datav)
      , mField(m_field){
    sYmm = true;
  }

  MoFEM::Interface &mField;
  boost::shared_ptr<PreviousData> &dataU;
  boost::shared_ptr<PreviousData> &dataV;

  FUval DRhs_u;

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                            EntityType col_type, EntData &row_data,
                            EntData &col_data) {
    MoFEMFunctionBegin;

    const int nn_row_dofs = row_data.getIndices().size();
    const int nn_col_dofs = col_data.getIndices().size();
    if (nn_row_dofs && nn_col_dofs) {
      const int nb_row_dofs = get_nb_dofs(row_data, thMaxOrder, mField);
      const int nb_col_dofs = get_nb_dofs(col_data, thMaxOrder, mField);
      mat.resize(nn_row_dofs, nn_col_dofs, false);
      mat.clear();

      const bool on_block_diag = ((row_side == col_side) && (row_type == col_type));
      if (on_block_diag) {
        int ii = nb_row_dofs;
        for (; ii < nn_row_dofs; ++ii)
          mat(ii, ii) = 1.0;
      }

      const int nb_integration_pts = getGaussPts().size2();

      
      auto t_u_value = getFTensor0FromVec(dataU->mass_values);
      auto t_v_value = getFTensor0FromVec(dataV->mass_values);

      auto t_w = getFTensor0IntegrationWeight();
      const double ts_a = getFEMethod()->ts_a;
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_v_base = row_data.getFTensor0N(gg, 0);
        const double a = vol * t_w;
        double dfu = DRhs_u(t_u_value, t_v_value);
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_v_base = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            mat(rr, cc) += ((ts_a - 0*dfu) * t_row_v_base * t_col_v_base) * a;

            ++t_col_v_base;
          }
          ++t_row_v_base;
          
        }
        ++t_w;
        ++t_u_value;
        ++t_v_value;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
      // if (row_side != col_side || row_type != col_type) {
      //   transMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(transMat) = trans(mat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &transMat(0, 0), ADD_VALUES);
      // }
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat, transMat;
};

struct OpPostError : public OpFaceEle {
  typedef boost::function<double(const double, const double)> FUval;

  OpPostError(std::string                     post_field_name,
              std::string                     mass_name,
              boost::shared_ptr<PreviousData> &data_u,
              boost::shared_ptr<PreviousData> &data_v,
              FUval                           rhs_u,
              double                          &cum_error,
              Range                           &stim_region,
              MoFEM::Interface                &m_field)
      : OpFaceEle(post_field_name, OpFaceEle::OPROW)
      , dataU(data_u)
      , dataV(data_v)
      , cumError(cum_error)
      , mField(m_field)
      , massName(mass_name)
      , stimRegion(stim_region)
      , rhsU(rhs_u){}
  Range &stimRegion;
  std::string massName;
  boost::shared_ptr<PreviousData> dataU;
  boost::shared_ptr<PreviousData> dataV;

  FUval  rhsU;

  double &cumError;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nn_dofs = data.getFieldData().size();
    // cout << "nb_error_dofs : " << nb_dofs << endl;
    EntityHandle fe_ent = getFEEntityHandle();
    if (nn_dofs) {
      const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);

      auto t_u_value = getFTensor0FromVec(dataU->mass_values);
      auto t_v_value = getFTensor0FromVec(dataV->mass_values);

      auto t_flux_value = getFTensor1FromMat<3>(dataU->flux_values);

      auto t_mass_dot = getFTensor0FromVec(dataU->mass_dots);
      auto t_flux_div = getFTensor0FromVec(dataU->flux_divs);

      auto t_mass_grad = getFTensor1FromMat<2>(dataU->mass_grads);

      const double vol = getMeasure();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();

  
      auto t_coords = getFTensor1CoordsAtGaussPts();

      const double c_time = getFEMethod()->ts_t;

      double dt;
      CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);

      double stim = 0.0;

      double T = time_stimulus;
      double duration = duration_stimulus;

      if (T - dt < c_time && c_time <= T + duration){
        EntityHandle stim_ent = getFEEntityHandle();
        if (stimRegion.find(stim_ent) != stimRegion.end()) {
          stim = 40.0;
        } else {
          stim = 0.0;
        }
      }

      FTensor::Tensor1<double, 3> t_constitutive_error;
      double eta1 = 0.0;
      double eta2 = 0.0;
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        double rhsu = rhsU(t_u_value, t_v_value);

        
        t_constitutive_error(0) = t_flux_value(0) + block.B0 * t_mass_grad(0);
        t_constitutive_error(1) = t_flux_value(1) + block.B0 * t_mass_grad(1);
        t_constitutive_error(2) = 0.0;

        eta1 += a * pow(t_mass_dot + t_flux_div - rhsu - factor * stim, 2);
        eta2 += a * t_constitutive_error(i) * t_constitutive_error(i);
        ++t_w;
        ++t_flux_div;
        ++t_flux_value;
        ++t_mass_dot;
        ++t_mass_grad;
        ++t_u_value;
        ++t_v_value;
        ++t_coords;
      }

      data.getFieldDofs()[0]->getFieldData() += sqrt(eta1) + sqrt(eta2);
      cumError += eta1 + eta2;


    }
    MoFEMFunctionReturn(0);
  }

private:
  MoFEM::Interface &mField;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
};

struct Monitor : public FEMethod {
  double &pError;
  double &maxError;
  MoFEM::Interface &mField;
  Monitor(MPI_Comm                                     &comm, 
          const int                                    &rank, 
          SmartPetscObj<DM>                            &dm,
          SmartPetscObj<TS>                            &ts,
          boost::shared_ptr<FaceEle>                   &domain_pipeline,
          boost::shared_ptr<PostProcFaceOnRefinedMesh> &post_proc, 
          double                                       &p_error,
          double                                       &max_error,
          MoFEM::Interface                             &m_field)
      : cOmm(comm)
      , rAnk(rank)
      , dM(dm)
      , tS(ts)
      , postProc(post_proc)
      , domainPipeline(domain_pipeline)
      , pError(p_error)
      , maxError(max_error)
      , mField(m_field)
      {};
  MoFEMErrorCode preProcess() { 
    MoFEMFunctionBegin;
    MoFEMFunctionReturn(0);
    }
  MoFEMErrorCode operator()() { return 0; }
  MoFEMErrorCode postProcess() {
    MoFEMFunctionBegin;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-save_every_nth_step",
                               &save_every_nth_step, PETSC_NULL);
    if (ts_step % save_every_nth_step == 0) {
      
      CHKERR DMoFEMLoopFiniteElements(dM, "dFE", postProc);
      CHKERR postProc->writeFile(
          "out_level_s" + boost::lexical_cast<std::string>(ts_step) + ".h5m");
    }
    CHKERR DMoFEMLoopFiniteElements(dM, "dFE", domainPipeline);

    double post_error_sum = 0;
    double error_max = 0;

    Vec post_error_proc;
    Vec vector_error_max_proc;


    CHKERR VecCreateMPI(cOmm, 1, PETSC_DECIDE, &post_error_proc);
    CHKERR VecCreateMPI(cOmm, 1, PETSC_DECIDE, &vector_error_max_proc);

    auto get_global_error = [&]() {
      MoFEMFunctionBegin;
      CHKERR VecSetValue(post_error_proc, rAnk, pError, INSERT_VALUES);
      CHKERR VecSetValue(vector_error_max_proc, rAnk, maxError, INSERT_VALUES);
      MoFEMFunctionReturn(0);
    };
    CHKERR get_global_error();

    CHKERR VecAssemblyBegin(post_error_proc);
    CHKERR VecAssemblyEnd(post_error_proc);

    CHKERR VecAssemblyBegin(vector_error_max_proc);
    CHKERR VecAssemblyEnd(vector_error_max_proc);


    CHKERR VecSum(post_error_proc, &post_error_sum);
    CHKERR VecMax(vector_error_max_proc, PETSC_NULL, &error_max);

    double postError = sqrt(post_error_sum);

    CHKERR PetscPrintf(PETSC_COMM_WORLD, "Post error      : %3.10e \n", postError);
    // CHKERR PetscPrintf(PETSC_COMM_WORLD, "Max local error : %3.10e \n", error_max);
    auto poly_adapt = [&]() {
      // setting order of a face
      for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(mField, "ERROR", dof)) {
        double hi_theta = 1;
        double lo_theta = 0;
        CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-hi_theta", &hi_theta,
                                   PETSC_NULL);
        CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-lo_theta", &lo_theta,
                                   PETSC_NULL);

        double eta_k = dof->get()->getFieldData();

        auto face_ent = dof->get()->getEnt();

        bool lo_bound = (eta_k < error_max * lo_theta) ? true : false;
        bool hi_bound = (eta_k > error_max * hi_theta) ? true : false;

        int pOrder;
        CHKERR mField.get_moab().tag_get_data(thMaxOrder, &face_ent, 1,
                                              &pOrder);
        
        if (lo_bound)
          pOrder = std::max(min_order, pOrder - 1);
        else if (hi_bound)
          pOrder = std::min(max_order, pOrder + 1);

        CHKERR mField.get_moab().tag_set_data(thMaxOrder, &face_ent, 1,
                                              &pOrder);

        dof->get()->getFieldData() = 0.0;
      }


      Range all_edge_entities;
      Range all_face_entities;
      CHKERR mField.get_moab().get_entities_by_type(0, MBEDGE,
                                                    all_edge_entities, false);
      CHKERR mField.get_moab().get_entities_by_type(0, MBTRI, all_face_entities,
                                                    false);

      for(auto edge : all_edge_entities){
        Range ad_faces;
        
        CHKERR mField.get_moab().get_adjacencies(&edge, 1, 2, false, ad_faces,
                                                 moab::Interface::UNION);
        int face_o[2];
        int n = 0;                                         
        if(ad_faces.size() == 2){
          for(auto f : ad_faces){
            CHKERR mField.get_moab().tag_get_data(thMaxOrder, &f, 1,
                                                  &face_o[n]);
            ++n;
          }
          if(abs(face_o[0] - face_o[1]) > 1){
            if(face_o[0] > face_o[1]){
              face_o[1] = face_o[0] - 1;
            } else
             face_o[0] = face_o[1] - 1;
            
          }
          int jj = 0;
          for (auto f : ad_faces) {
            CHKERR mField.get_moab().tag_set_data(thMaxOrder, &f, 1, &face_o[jj]);
            ++jj;
          }
        }
      }

      // order an edge must be max of adjacent face orders
      for (auto edge : all_edge_entities) {
        int edge_order = 1;
        Range adj_faces;
        CHKERR mField.get_moab().get_adjacencies(&edge, 1, 2, false, adj_faces,
                                                 moab::Interface::UNION);
        // CHKERR PetscPrintf(PETSC_COMM_WORLD, "size of adj_faces : %2d \n",
        // adj_faces.size());
        for (auto face : adj_faces) {
          int face_order;
          CHKERR mField.get_moab().tag_get_data(thMaxOrder, &face, 1,
                                                &face_order);
          edge_order = (edge_order > face_order) ? edge_order : face_order;
        }
        CHKERR mField.get_moab().tag_set_data(thMaxOrder, &edge, 1,
                                              &edge_order);
      }
      ParallelComm *pcomm =
          ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);

      CHKERR pcomm->reduce_tags(thMaxOrder, MPI_MAX, all_edge_entities);

      // set integration rule for face max order of the three adjacent edges
      for (auto face : all_face_entities) {
        int face_rule;
        CHKERR mField.get_moab().tag_get_data(thMaxOrder, &face, 1, &face_rule);
        Range adj_edges;
        CHKERR mField.get_moab().get_adjacencies(&face, 1, 1, false, adj_edges,
                                                 moab::Interface::UNION);
        // cout << "size of adj_edge : " << adj_edges.size() << endl;

        for (auto edge : adj_edges) {
          int edge_order;
          CHKERR mField.get_moab().tag_get_data(thMaxOrder, &edge, 1,
                                                &edge_order);

          face_rule = (face_rule > edge_order) ? face_rule : edge_order;
        }
        CHKERR mField.get_moab().tag_set_data(thQuadRule, &face, 1, &face_rule);

        // cout << "face_rule : " << face_rule << endl;
      }
    };

    auto active_dofs = [&]() {
        const MoFEM::Problem *problem_ptr;
        CHKERR DMMoFEMGetProblemPtr(dM, &problem_ptr);
        auto dofs = problem_ptr->getNumeredRowDofs();
        auto nb_local_dofs = problem_ptr->getNbLocalDofsRow();


        std::vector<int> vec_is_lo;


        vec_is_lo.reserve(dofs->size());


        for (auto &dof : *(dofs)) {
          auto loc_idx = dof->getPetscLocalDofIdx();

          if (loc_idx < nb_local_dofs) {
            auto ent = dof->getEnt();
            int tag_order;
            CHKERR mField.get_moab().tag_get_data(thMaxOrder, &ent, 1, &tag_order); 
            
            auto ord = dof->getDofOrder();

            bool correct_order = false;

            if(dof->getSpace() == L2){

                correct_order = (ord < tag_order);
            } else {

                correct_order = (ord <= tag_order);
            }

            if (correct_order){
              vec_is_lo.push_back(dof->getPetscGlobalDofIdx());
            } 

          }
        }
        double size = 0;
        double size_i = (double) vec_is_lo.size();
        Vec active_dofs_per_proc;
        CHKERR VecCreateMPI(cOmm, 1, PETSC_DECIDE, &active_dofs_per_proc);
        CHKERR VecSetValue(active_dofs_per_proc, rAnk, size_i, INSERT_VALUES);

        CHKERR VecAssemblyBegin(active_dofs_per_proc);
        CHKERR VecAssemblyEnd(active_dofs_per_proc);

        CHKERR VecSum(active_dofs_per_proc, &size);

        CHKERR PetscPrintf(PETSC_COMM_WORLD, "Active dofs    : %3d \n", (int)size);
    };
 

    if ( adaptive && (ts_step >= 0) ){
      poly_adapt();

    }
    active_dofs();

    for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(mField, "ORDER", dof)) {
      auto face_ent = dof->get()->getEnt();
      int face_order;
      CHKERR mField.get_moab().tag_get_data(thMaxOrder, &face_ent, 1,
                                            &face_order);
      dof->get()->getFieldData() = (double)face_order;
    }


    pError = 0.0;
    maxError = 0.0;





    MoFEMFunctionReturn(0);
  }

private:
  SmartPetscObj<DM>                            dM;
  SmartPetscObj<TS>                             tS;
  boost::shared_ptr<PostProcFaceOnRefinedMesh> postProc;
  boost::shared_ptr<FaceEle>                   domainPipeline;
  MPI_Comm                                     cOmm;
  const int                                    rAnk;
};

}; // namespace ElectroPhysiology

#endif //__ELECPHYSOPERATORS_HPP__