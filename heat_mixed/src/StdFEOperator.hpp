#ifndef __STDFEOPERATOR_HPP__
#define __STDFEOPERATOR_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>
#include <FEOperator.hpp>

namespace StdOperators {

using namespace Operators;  

// // using StdFaceEle = MoFEM::FaceElementForcesAndSourcesCore;
// using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
//                     FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
//                     FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
//                     FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

// // using StdEdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;
// using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
//                     EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
//                     EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

// using OpFaceEle = FaceEle::UserDataOperator;
// using OpEdgeEle = EdgeEle::UserDataOperator;

// using EntData = DataForcesAndSourcesCore::EntData;

// using PostProc = PostProcFaceOnRefinedMesh;

// const double K = 1e-2;
// int save_every_nth_step = 1;

// struct CommonData {
//   MatrixDouble jAc;
//   MatrixDouble invJac;

//   boost::shared_ptr<VectorDouble> uDotPtr;
//   boost::shared_ptr<MatrixDouble> uGradPtr;

//   CommonData() {
//     jAc.resize(3, 3, false);
//     invJac.resize(3, 3, false);

//     uDotPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
//     uGradPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
//   }
// }; // CommonData

struct OpRhsH1 : OpFaceEle {
  OpRhsH1(std::string first_field,
          boost::shared_ptr<CommonData> common_data_ptr,
          BlockData                     &block_data,
          Range                        *ents = NULL)
      : OpFaceEle(first_field, OpFaceEle::OPROW),
        commonDataPtr(common_data_ptr), 
        blockData(block_data),
        eNts(ents) {}

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  VectorDouble loc_rhs;
  BlockData    &blockData;

  MoFEMErrorCode doWork(int side, EntityType type, EntData &row_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();

    if (!nb_row_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_rhs.resize(nb_row_dofs, false);
    loc_rhs.clear();

    auto t_u_grad = getFTensor1FromMat<2>(*commonDataPtr->freeDrugGradPtr);
    auto t_u_dot = getFTensor0FromVec(*commonDataPtr->freeDrugDotPtr);

    double kr = blockData.radialDiffuse;
    double kz = blockData.axialDiffuse;

    auto t_K = FTensor::Tensor2<double, 2, 2>(kr,  0.0,
                                               0.0, kz);


    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    const double vol = getMeasure();

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;

    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      auto t_row_grad = row_data.getFTensor1DiffN<2>(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        loc_rhs[rr] += (t_row_shape * t_u_dot + t_row_grad(i) * t_K(i, j) * t_u_grad(j)) * a;
        ++t_row_shape;
        ++t_row_grad;
      }
      ++t_u_grad;
      ++t_u_dot;
      ++t_w;
    }

    CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                        PETSC_TRUE);
    CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*loc_rhs.begin(),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpRhsH1

struct OpRhsH1Field2 : OpFaceEle {
  OpRhsH1Field2(std::string first_field,
                boost::shared_ptr<CommonData> common_data_ptr, Range *ents = NULL)
      : OpFaceEle(first_field, OpFaceEle::OPROW),
        commonDataPtr(common_data_ptr), eNts(ents) {}

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  VectorDouble loc_rhs;

  MoFEMErrorCode doWork(int side, EntityType type, EntData &row_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();

    if (!nb_row_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_rhs.resize(nb_row_dofs, false);
    loc_rhs.clear();
    auto t_u_dot = getFTensor0FromVec(*commonDataPtr->boundDrugDotPtr);

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    const double vol = getMeasure();


    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        loc_rhs[rr] += t_row_shape * t_u_dot * a;
        ++t_row_shape;
      }
      ++t_u_dot;
      ++t_w;
    }

    CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES, PETSC_TRUE);
    CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*loc_rhs.begin(), ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpRhsH1Field2

struct OpLhsH1_H1 : OpFaceEle {
  OpLhsH1_H1(std::string first_field, 
             std::string second_field,
             BlockData   &block_data,
             boost::shared_ptr<CommonData> common_data_ptr, Range *ents = NULL)

      : OpFaceEle(first_field, second_field, OpFaceEle::OPROWCOL),
        commonDataPtr(common_data_ptr), 
        blockData(block_data),
        eNts(ents) {
    sYmm = false;
  }

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  MatrixDouble loc_lhs;

  BlockData  &blockData;

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (!nb_row_dofs || !nb_col_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_lhs.resize(nb_row_dofs, nb_col_dofs, false);
    loc_lhs.clear();

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();

    const double vol = getMeasure();

    double kr = blockData.radialDiffuse;
    double kz = blockData.axialDiffuse;

    auto t_K = FTensor::Tensor2<double, 2, 2>(kr,  0.0,
                                              0.0, kz);

    FTensor::Index<'i', 2> i;
    FTensor::Index<'j', 2> j;
    const double ts_shift = getFEMethod()->ts_a;

    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      auto t_row_grad = row_data.getFTensor1DiffN<2>(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        auto t_col_shape = col_data.getFTensor0N(gg, 0);
        auto t_col_grad = col_data.getFTensor1DiffN<2>(gg, 0);
        for (int cc = 0; cc != nb_col_dofs; ++cc) {
          loc_lhs(rr, cc) += (ts_shift * t_row_shape * t_col_shape + t_row_grad(i) * t_K(i, j) * t_col_grad(j)) * a;
          ++t_col_shape;
          ++t_col_grad;
        }
        ++t_row_grad;
        ++t_row_shape;
      }
      ++t_w;
    }

    CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &loc_lhs(0, 0),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpLhsH1_H1

struct OpLhsField2H1_H1 : OpFaceEle {
  OpLhsField2H1_H1(std::string first_field, 
                   std::string second_field,
                   boost::shared_ptr<CommonData> common_data_ptr, 
                   Range *ents = NULL)

      : OpFaceEle(first_field, second_field, OpFaceEle::OPROWCOL),
        commonDataPtr(common_data_ptr), 
        eNts(ents) {
    sYmm = false;
  }

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  MatrixDouble loc_lhs;


  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (!nb_row_dofs || !nb_col_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_lhs.resize(nb_row_dofs, nb_col_dofs, false);
    loc_lhs.clear();

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();

    const double vol = getMeasure();


    const double ts_shift = getFEMethod()->ts_a;

    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        auto t_col_shape = col_data.getFTensor0N(gg, 0);
        for (int cc = 0; cc != nb_col_dofs; ++cc) {
          loc_lhs(rr, cc) += ts_shift * t_row_shape * t_col_shape * a;
          ++t_col_shape;
        }
        ++t_row_shape;
      }
      ++t_w;
    }

    CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &loc_lhs(0, 0),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpLhsField2H1_H1

struct OpRhsH1Explicit : OpFaceEle {
  typedef boost::function<void(VectorDouble &, MatrixDouble &)> VectorFunction;
  OpRhsH1Explicit(std::string     field_name,
                  VectorFunction  vec_func,
                  Range           *ents = NULL)
        : OpFaceEle(field_name, OpFaceEle::OPROW)
        , vecFunc(vec_func)
        , eNts(ents){}

  Range *eNts;
  VectorDouble  src_vec;
  VectorFunction  vecFunc;
  VectorDouble   loc_rhs;


  MoFEMErrorCode doWork(int side, EntityType type, EntData &row_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();

    if (!nb_row_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_rhs.resize(nb_row_dofs, false);
    loc_rhs.clear();


    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    const double vol = getMeasure();

    auto gp_coords = getCoordsAtGaussPts();
    vecFunc(src_vec, gp_coords);

    auto t_src = getFTensor0FromVec(src_vec);

    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        loc_rhs[rr] += t_row_shape * t_src * a;
        ++t_row_shape;
      }
      ++t_src;
      ++t_w;
    }
    CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                        PETSC_TRUE);
    CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*loc_rhs.begin(),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
};

struct OpLocH1MassMatrix : OpFaceEle {
    OpLocH1MassMatrix(std::string                     field_name,
                      SmartPetscObj<Mat>              mass_matrix,
                      Range                           *ents = NULL)
        : OpFaceEle(field_name, field_name, OpFaceEle::OPROWCOL),
          massMatrix(mass_matrix), eNts(ents) 
          {
            sYmm = true;  
          }

    MoFEMErrorCode doWork(int        row_side, 
                          int        col_side, 
                          EntityType row_type,
                          EntityType col_type, 
                          EntData    &row_data,
                          EntData    &col_data) {
      MoFEMFunctionBegin;
      const int nb_row_dofs = row_data.getIndices().size();
      const int nb_col_dofs = col_data.getIndices().size();
      if (!nb_row_dofs || !nb_col_dofs)
        MoFEMFunctionReturnHot(0);

      if (eNts) {
        if (eNts->find(getFEEntityHandle()) == eNts->end())
          MoFEMFunctionReturnHot(0);
      }



      loc_mass.resize(nb_row_dofs, nb_row_dofs, false);
      loc_mass.clear();

      const int nb_gauss_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      auto t_coords = getFTensor1CoordsAtGaussPts();

      for (int gg = 0; gg != nb_gauss_pts; ++gg) {
        const double a = vol * t_w;
        auto t_row_shape = row_data.getFTensor0N(gg, 0);
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_shape = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            loc_mass(rr, cc) += t_row_shape * t_col_shape * a;
            ++t_col_shape;
          }
          ++t_row_shape;
        }
        ++t_w;
      }

      CHKERR MatSetValues(massMatrix, row_data, col_data, &loc_mass(0, 0), ADD_VALUES);

      if (row_side != col_side || row_type != col_type) {
        trans_loc_mass.resize(nb_col_dofs, nb_row_dofs, false);
        noalias(trans_loc_mass) = trans(loc_mass);
        CHKERR MatSetValues(massMatrix, col_data, row_data, &trans_loc_mass(0, 0), ADD_VALUES);
      }

      MoFEMFunctionReturn(0);
    }

  private:
    Range *eNts;

    SmartPetscObj<Mat> massMatrix;
    MatrixDouble loc_mass;
    MatrixDouble trans_loc_mass;
  };

// struct Monitor : public FEMethod {
//   Monitor(Simple *simple_interface,
//           boost::shared_ptr<PostProc> &post_proc)
//       : postProc(post_proc), simpleInterface(simple_interface){};
//   MoFEMErrorCode preProcess() { return 0; }
//   MoFEMErrorCode operator()() { return 0; }

//   MoFEMErrorCode postProcess() {
//     MoFEMFunctionBegin;
//     CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-save_every_nth_step",
//                               &save_every_nth_step, PETSC_NULL);
//     if (ts_step % save_every_nth_step == 0) {

//       CHKERR DMoFEMLoopFiniteElements(simpleInterface->getDM(),
//                                       simpleInterface->getDomainFEName(),
//                                       postProc);
//       CHKERR postProc->writeFile(
//           "output_std_" + boost::lexical_cast<std::string>(ts_step) + ".h5m");
//     }

//     MoFEMFunctionReturn(0);
//   }

// private:
//   boost::shared_ptr<PostProc> postProc;
//   Simple *simpleInterface;
// }; // Monitor

}; // namespace Operators

#endif