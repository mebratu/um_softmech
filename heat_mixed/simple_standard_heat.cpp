#include <stdlib.h>
#include <BasicFiniteElements.hpp>
#include <StdFEOperator.hpp>
#include <FEOperator.hpp>
#include <FETools.hpp>

static char help[] = "...\n\n";

using namespace MoFEM;
using namespace StdOperators;
using namespace Operators;

double initVal = 1;

struct FEProblem {
public:
  FEProblem(MoFEM::Interface &m_field);

  MoFEMErrorCode runAnalysis();

private:
  MoFEMErrorCode setupInput();
  MoFEMErrorCode findBlocks();
  MoFEMErrorCode setupFields();
  MoFEMErrorCode setupIntegrationRule();
  MoFEMErrorCode applyBIcondition();
  MoFEMErrorCode updateElements(boost::shared_ptr<FaceEle> &fe_ele);
  MoFEMErrorCode setupFETSsystem();
  MoFEMErrorCode setupAssemblySystem();
  MoFEMErrorCode setupPostProcess();
  MoFEMErrorCode executeSetups();

  MoFEM::Interface &mField;
  Simple *simpleInterface;

  SmartPetscObj<DM> dM;
  SmartPetscObj<TS> tS;

  Range intialValueEnts;
  Range essentialBoundEnts;

  DataValues dataValues;

  std::map<std::string, BlockData> setOfBlocks;

  SmartPetscObj<Mat> massMatrix;
  SmartPetscObj<KSP> massKsp;



  int oRder;

  boost::shared_ptr<FaceEle> facePipelineNonStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffLhs;

  boost::shared_ptr<FaceEle> facePipelineInitial;

  boost::shared_ptr<FaceEle> massMatrixPipeline;

  boost::shared_ptr<PostProc> postProc;
  boost::shared_ptr<Monitor> monitorPtr;

  boost::shared_ptr<CommonData> commonData;

  boost::shared_ptr<ForcesAndSourcesCore> null;
}; // FEProblem

FEProblem::FEProblem(MoFEM::Interface &m_field) : mField(m_field), oRder(2) {
  facePipelineNonStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineStiffLhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineInitial = boost::shared_ptr<FaceEle>(new FaceEle(m_field));

  massMatrixPipeline = boost::shared_ptr<FaceEle>(new FaceEle(m_field));

  postProc = boost::shared_ptr<PostProc>(new PostProc(m_field));

  commonData = boost::shared_ptr<CommonData>(new CommonData());
}

MoFEMErrorCode FEProblem::setupInput() {
  MoFEMFunctionBegin;
  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::findBlocks() {
  MoFEMFunctionBegin;
  auto get_radial_cond = [](std::string block_name) {
    double kr;
    if (block_name == "COAT") {
      kr = D_c;
    } else if (block_name == "WALL") {
      kr = Dm_r;
    } else {
      kr = K;
    }
    return Dm_z;
  };

  auto get_axial_cond = [](std::string block_name) {
    double kz;
    if (block_name == "COAT") {
      kz = D_c;
    } else if (block_name == "WALL") {
      kz = Dm_z;
    } else {
      kz = Dm_z;
    }
    return kz;
  };

  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    std::string name = it->getName();

    const int id = it->getMeshsetId();
    setOfBlocks[name].iD = id;
    if (name.compare(0, 4, "COAT") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      double kr = (timeScale / (lengthScale * lengthScale)) * get_radial_cond(name);
      double kz = (timeScale / (lengthScale * lengthScale)) * get_axial_cond(name);
      setOfBlocks[name].radialDiffuse = kr;
      setOfBlocks[name].axialDiffuse = kz;
    }if (name.compare(0, 4, "WALL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      double kr = (timeScale / (lengthScale * lengthScale)) * get_radial_cond(name);
      double kz = (timeScale / (lengthScale * lengthScale)) * get_axial_cond(name);
      setOfBlocks[name].radialDiffuse = kr;
      setOfBlocks[name].axialDiffuse = kz;
    }
     else if (name.compare(0, 7, "INITIAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, intialValueEnts, true);
      std::vector<double> init_val;
      it->getAttributes(init_val);
      setOfBlocks[name].initVal = 1.0;
    } else if (name.compare(0, 7, "NATURAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 1, essentialBoundEnts, true);
    }
  }
  MoFEMFunctionReturn(0);
}
MoFEMErrorCode FEProblem::setupFields() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField("FreeDrug", H1, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDomainField("BoundDrug", H1, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &oRder, PETSC_NULL);

  CHKERR simpleInterface->setFieldOrder("FreeDrug", oRder);
  CHKERR simpleInterface->setFieldOrder("BoundDrug", oRder);

  CHKERR simpleInterface->setUp();

  dM = simpleInterface->getDM();
  tS = createTS(mField.get_comm());
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::setupIntegrationRule() {
  MoFEMFunctionBegin;
  auto vol_rule = [](int, int, int p) -> int { return 2 * p; };

  facePipelineNonStiffRhs->getRuleHook = vol_rule;
  facePipelineStiffRhs->getRuleHook = vol_rule;
  facePipelineStiffLhs->getRuleHook = vol_rule;

  massMatrixPipeline->getRuleHook = vol_rule;
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::applyBIcondition() {
  MoFEMFunctionBegin;
  if (!intialValueEnts.empty()) {
    Range init_verts;
    CHKERR mField.get_moab().get_connectivity(intialValueEnts, init_verts, false);
    CHKERR mField.getInterface<FieldBlas>()->setField(initVal, MBVERTEX, init_verts, "FreeDrug");
  }

  // CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
  //     "SimpleProblem", "U", sigmaBoundEnts);

  CHKERR DMCreateMatrix_MoFEM(dM, massMatrix);

  CHKERR MatZeroEntries(massMatrix);

  massMatrixPipeline->getOpPtrVector().push_back(new OpLocH1MassMatrix("FreeDrug", massMatrix));
  massMatrixPipeline->getOpPtrVector().push_back(new OpLocH1MassMatrix("BoundDrug", massMatrix));

  CHKERR DMoFEMLoopFiniteElements(dM, simpleInterface->getDomainFEName(), massMatrixPipeline);

  CHKERR MatAssemblyBegin(massMatrix, MAT_FINAL_ASSEMBLY);
  CHKERR MatAssemblyEnd(massMatrix, MAT_FINAL_ASSEMBLY);

  // Create and septup KSP (linear solver), we need this to calculate g(t,u) =
  // M^-1G(t,u)
  massKsp = createKSP(mField.get_comm());
  CHKERR KSPSetOperators(massKsp, massMatrix, massMatrix);
  CHKERR KSPSetFromOptions(massKsp);
  CHKERR KSPSetUp(massKsp);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::updateElements(boost::shared_ptr<FaceEle> &fe_ele) {
  MoFEMFunctionBegin;
  fe_ele->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));
  fe_ele->getOpPtrVector().push_back(
      new OpSetInvJacH1ForFace(commonData->invJac));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::setupFETSsystem() {
  MoFEMFunctionBegin;

  // Explicit Rhs
  CHKERR updateElements(facePipelineNonStiffRhs);
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("FreeDrug", commonData->freeDrugValPtr));

  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("BoundDrug", commonData->boundDrugValPtr));


  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpRhsH1Explicit("BoundDrug", OpBoundDrugSrc(commonData), &setOfBlocks["WALL"].eNts));

  auto solve_for_g = [&]() {
    MoFEMFunctionBegin;
    if (facePipelineNonStiffRhs->vecAssembleSwitch) {
      CHKERR VecGhostUpdateBegin(facePipelineNonStiffRhs->ts_F, ADD_VALUES,
                                 SCATTER_REVERSE);
      CHKERR VecGhostUpdateEnd(facePipelineNonStiffRhs->ts_F, ADD_VALUES,
                               SCATTER_REVERSE);
      CHKERR VecAssemblyBegin(facePipelineNonStiffRhs->ts_F);
      CHKERR VecAssemblyEnd(facePipelineNonStiffRhs->ts_F);
      *facePipelineNonStiffRhs->vecAssembleSwitch = false;
    }
    CHKERR KSPSolve(massKsp, facePipelineNonStiffRhs->ts_F,
                             facePipelineNonStiffRhs->ts_F);
    MoFEMFunctionReturn(0);
  };

  // Add hook to the element to calculate g.
  facePipelineNonStiffRhs->postProcessHook = solve_for_g;

  // Implicit Rhs
  CHKERR updateElements(facePipelineStiffRhs);

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot("FreeDrug", commonData->freeDrugDotPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot("BoundDrug", commonData->boundDrugDotPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldGradient<2>("FreeDrug", commonData->freeDrugGradPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRhsH1("FreeDrug", commonData, setOfBlocks["COAT"],  &setOfBlocks["COAT"].eNts));
  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRhsH1("FreeDrug", commonData, setOfBlocks["WALL"],  &setOfBlocks["WALL"].eNts));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRhsH1Field2("BoundDrug", commonData));

  // Stiff Lhs
  CHKERR updateElements(facePipelineStiffLhs);

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpLhsH1_H1("FreeDrug", "FreeDrug", setOfBlocks["COAT"], commonData, &setOfBlocks["COAT"].eNts));
  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpLhsH1_H1("FreeDrug", "FreeDrug", setOfBlocks["WALL"], commonData, &setOfBlocks["WALL"].eNts));

  facePipelineStiffLhs->getOpPtrVector().push_back( 
      new OpLhsField2H1_H1("BoundDrug", "BoundDrug", commonData));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::setupAssemblySystem() {
  MoFEMFunctionBegin;
  CHKERR TSSetType(tS, TSARKIMEX);
  CHKERR TSARKIMEXSetType(tS, TSARKIMEXA2);

  CHKERR DMMoFEMTSSetRHSFunction(dM, simpleInterface->getDomainFEName(),
                               facePipelineNonStiffRhs, null, null);

  CHKERR DMMoFEMTSSetIFunction(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffRhs, null, null);

  CHKERR DMMoFEMTSSetIJacobian(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffLhs, null, null);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::setupPostProcess() {
  MoFEMFunctionBegin;
  postProc->generateReferenceElementMesh();

  postProc->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));
  postProc->getOpPtrVector().push_back(
      new OpSetInvJacH1ForFace(commonData->invJac));

  postProc->addFieldValuesPostProc("FreeDrug");
  postProc->addFieldValuesPostProc("BoundDrug");

  auto facePipelineIntegrateField = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  CHKERR updateElements(facePipelineIntegrateField);

  facePipelineIntegrateField->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("FreeDrug", commonData->freeDrugValPtr));

  facePipelineIntegrateField->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("BoundDrug", commonData->boundDrugValPtr));

  boost::shared_ptr<VectorDouble> null_vector;

  auto calc_data_value = [&](auto operator_func, double &data_val, Range *entsPtr = NULL) {
    facePipelineIntegrateField->getOpPtrVector().push_back(
        new FeTools::OpIntegrateData("FreeDrug", data_val, operator_func, mField, entsPtr));
  };

  calc_data_value(OpVolume(commonData), dataValues.CoatingVolume, &setOfBlocks["COAT"].eNts);
  calc_data_value(OpVolume(commonData), dataValues.WallVolume, &setOfBlocks["WALL"].eNts);
  calc_data_value(OpFreeDrug(commonData), dataValues.FreeDrugCoating, &setOfBlocks["COAT"].eNts);
  calc_data_value(OpFreeDrug(commonData), dataValues.FreeDrugWall, &setOfBlocks["WALL"].eNts);
  calc_data_value(OpBoundDrug(commonData), dataValues.BoundDrugWall, &setOfBlocks["WALL"].eNts);
  calc_data_value(OpTotalDrug(commonData), dataValues.TotalDrugContent);
  calc_data_value(OpSquareDrug(commonData), dataValues.SquareDrugContent);



  monitorPtr = boost::shared_ptr<Monitor>(new Monitor(
      simpleInterface, facePipelineIntegrateField, postProc, mField, &dataValues));

  CHKERR DMMoFEMTSSetMonitor(dM, tS, simpleInterface->getDomainFEName(),
                             monitorPtr, null, null);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::executeSetups() {
  MoFEMFunctionBegin;

  TSAdapt adapt;

  // Create solution vector
  SmartPetscObj<Vec> X;
  CHKERR DMCreateGlobalVector_MoFEM(dM, X);
  CHKERR DMoFEMMeshToLocalVector(dM, X, INSERT_VALUES, SCATTER_FORWARD);
  // Solve FEproblem
  double ftime, dt;
  ftime = 1;
  CHKERR TSSetDM(tS, dM);
  CHKERR TSSetMaxTime(tS, ftime);
  CHKERR TSSetSolution(tS, X);
  // dt = 1e-5; /* Initial time step */
  // CHKERR TSSetTimeStep(tS, dt);

  // CHKERR TSGetAdapt(tS, &adapt);
  // CHKERR TSAdaptSetType(adapt, TSADAPTBASIC);
  // CHKERR TSAdaptSetStepLimits(
  //     adapt, 1e-12,
  //     1e-2); /* Also available with -ts_adapt_dt_min/-ts_adapt_dt_max */
  // CHKERR TSSetMaxSNESFailures(tS,
  //                             -1); /* Retry step an unlimited number of times
  //                             */
  CHKERR TSSetFromOptions(tS);
  CHKERR TSSolve(tS, X);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode FEProblem::runAnalysis() {
  MoFEMFunctionBegin;
  CHKERR setupInput();
  CHKERR findBlocks();
  CHKERR setupFields();
  CHKERR setupIntegrationRule();
  CHKERR applyBIcondition();
  CHKERR setupFETSsystem();
  CHKERR setupAssemblySystem();
  CHKERR setupPostProcess();
  CHKERR executeSetups();
  MoFEMFunctionReturn(0);
}

int main(int argc, char *argv[]) {

  const char param_file[] = "param_file.petsc";

  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);

    MoFEM::Interface &m_field = core;

   
    FEProblem simple_standard_equation(m_field);
    if (!data_fp)
      SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_SYS, "Cannot open file = %s \n",
               "data_file");
    PetscFPrintf(PETSC_COMM_SELF, data_fp, "time,CoatingVolume,WallVolume,FreeDrugCoating,FreeDrugWall,BoundDrugWall,TotalDrug,SquareDrug\n");
    simple_standard_equation.runAnalysis();
    if (data_fp) {
      fclose(data_fp);
      data_fp = NULL;
    }
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}